<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Imagekit Configuration
    |--------------------------------------------------------------------------
    |
    | All variables related to the imagekit.io
    |
    */

    'public_key' => env('IMAGEKIT_PUBLIC_KEY', ''),
    'private_key' => env('IMAGEKIT_PRIVATE_KEY', ''),
    'endpoint' => env('IMAGEKIT_ENDPOINT', ''),
];
