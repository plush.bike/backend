<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Frontend Configuration
    |--------------------------------------------------------------------------
    |
    | All variables related to the frontend of the application
    |
    */

    'app_url' => env('FRONTEND_APP_URL', 'http://localhost:3010'),
];
