<?php

use App\Enums\SocialProviderType;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/social-provider/{provider}/redirect', function ($provider) {
    if (!SocialProviderType::hasKey(strtoupper($provider))) {
        abort(404);
    }

    /** @var Laravel\Socialite\Two\AbstractProvider */
    $socialiteProvider = Socialite::driver(strtolower($provider));

    return $socialiteProvider->stateless()->redirect();
});
