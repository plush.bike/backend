<?php

namespace Database\Factories;

use App\Enums\SocialProviderType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\SocialProvider>
 */
class SocialProviderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'provider' => SocialProviderType::STRAVA,
            'provider_user_id' => '12345',
            'token' => null,
            'refresh_token' => null,
            'expires_at' => null,
        ];
    }
}
