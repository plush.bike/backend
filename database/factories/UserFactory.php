<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('test'),
            'remember_token' => Str::random(10),
        ];
    }

    public function twoFactorAuthenticationUnconfirmed()
    {
        return $this->state(function () {
            return [
                'two_factor_secret' =>
                    'eyJpdiI6IjFBTnhJRzNxR2hMVHJ5aWlQcVRuSHc9PSIsInZhbHVlIjoicTAyTHNXMFRCcUU2WVRwR0hYNFRLYlFtWnBIZmlTK3NnUUtkMkI3cFYyVT0iLCJtYWMiOiJiOGRlYTRjMjQwNzVhNDlmYzRlNTk3YWZiMDk0MmI2OWRiMjYxOTgzNDUyZmQ5NGExNWQwY2YwNzgzODlhZmNhIiwidGFnIjoiIn0=',
                'two_factor_recovery_codes' => encrypt(
                    json_encode([
                        'fake-recovery-code-1',
                        'fake-recovery-code-2',
                    ])
                ),
            ];
        });
    }

    public function twoFactorAuthenticationEnabled()
    {
        return $this->state(function () {
            return [
                'two_factor_secret' =>
                    'eyJpdiI6IjFBTnhJRzNxR2hMVHJ5aWlQcVRuSHc9PSIsInZhbHVlIjoicTAyTHNXMFRCcUU2WVRwR0hYNFRLYlFtWnBIZmlTK3NnUUtkMkI3cFYyVT0iLCJtYWMiOiJiOGRlYTRjMjQwNzVhNDlmYzRlNTk3YWZiMDk0MmI2OWRiMjYxOTgzNDUyZmQ5NGExNWQwY2YwNzgzODlhZmNhIiwidGFnIjoiIn0=',
                'two_factor_recovery_codes' => encrypt(
                    json_encode([
                        'fake-recovery-code-1',
                        'fake-recovery-code-2',
                    ])
                ),
                'two_factor_confirmed_at' => '2022-04-22 22:58:50',
            ];
        });
    }
}
