<?php

namespace Database\Factories;

use App\Models\Manufacturer;
use Illuminate\Database\Eloquent\Factories\Factory;

class ManufacturerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Manufacturer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
        ];
    }

    public function fox()
    {
        return $this->state(function () {
            return [
                'name' => 'Fox',
            ];
        });
    }

    public function rockShox()
    {
        return $this->state(function () {
            return [
                'name' => 'RockShox',
            ];
        });
    }
}
