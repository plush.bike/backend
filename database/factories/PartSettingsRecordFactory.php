<?php

namespace Database\Factories;

use App\Models\PartSettingsRecord;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartSettingsRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartSettingsRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nickname' => $this->faker->word(),
            'lsc_clicks' => $this->faker->numberBetween(1, 15),
            'hsc_clicks' => $this->faker->numberBetween(1, 15),
            'lsr_clicks' => $this->faker->numberBetween(1, 15),
            'hsr_clicks' => $this->faker->numberBetween(1, 15),
            'volume_spacers' => $this->faker->numberBetween(1, 4),
            'psi' => $this->faker->numberBetween(60, 100),
            'sag' => $this->faker->numberBetween(25, 30),
            'spring_rate' => 450,
            'preload_adjust' => $this->faker->numberBetween(8, 20),
            'notes' => $this->faker->sentences(3, true),
        ];
    }
}
