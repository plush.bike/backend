<?php

namespace Database\Factories;

use App\Enums\AirOrCoilType;
use App\Models\PartSpecificationsRecord;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartSpecificationsRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartSpecificationsRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lsc_max_clicks' => 15,
            'hsc_max_clicks' => 15,
            'lsr_max_clicks' => 15,
            'hsr_max_clicks' => 15,
            'travel' => $this->faker->numberBetween(120, 180),
            'air_or_coil' => AirOrCoilType::AIR,
        ];
    }
}
