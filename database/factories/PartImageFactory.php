<?php

namespace Database\Factories;

use App\Models\PartImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'src' => "parts/1/{$this->faker->uuid()}.png",
        ];
    }
}
