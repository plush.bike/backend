<?php

namespace Database\Factories;

use App\Enums\PartType;
use App\Models\PartSettingsRecord;
use App\Models\Part;
use App\Models\User;
use App\Models\Manufacturer;
use App\Models\PartSpecificationsRecord;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Part::class;

    protected function fox()
    {
        $fox = Manufacturer::where('name', '=', 'Fox')->first();
        return $fox ? $fox->id : Manufacturer::factory()->fox();
    }

    protected function rockShox()
    {
        $rockShox = Manufacturer::where('name', '=', 'RockShox')->first();
        return $rockShox ? $rockShox->id : Manufacturer::factory()->rockShox();
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'manufacturer_id' => Manufacturer::factory(),
            'name' => $this->faker->name,
            'type' => PartType::FORK,
            'active_settings_record_id' => null,
        ];
    }

    public function fox36()
    {
        return $this->state(function () {
            return [
                'manufacturer_id' => $this->fox(),
                'name' => '36',
                'type' => PartType::FORK,
            ];
        })
            ->has(PartSettingsRecord::factory()->count(2), 'settingsRecords')
            ->has(PartSpecificationsRecord::factory(), 'specificationsRecord');
    }

    public function foxFloatX2()
    {
        return $this->state(function () {
            return [
                'manufacturer_id' => $this->fox(),
                'name' => 'Float X2',
                'type' => PartType::SHOCK,
            ];
        })
            ->has(PartSettingsRecord::factory()->count(2), 'settingsRecords')
            ->has(PartSpecificationsRecord::factory(), 'specificationsRecord');
    }

    public function rockShoxLyric()
    {
        return $this->state(function () {
            return [
                'manufacturer_id' => $this->rockShox(),
                'name' => 'Lyric',
                'type' => PartType::FORK,
            ];
        })
            ->has(PartSettingsRecord::factory()->count(2), 'settingsRecords')
            ->has(PartSpecificationsRecord::factory(), 'specificationsRecord');
    }

    public function rockShoxZeb()
    {
        return $this->state(function () {
            return [
                'manufacturer_id' => $this->rockShox(),
                'name' => 'Zeb',
                'type' => PartType::FORK,
            ];
        })
            ->has(PartSettingsRecord::factory()->count(2), 'settingsRecords')
            ->has(PartSpecificationsRecord::factory(), 'specificationsRecord');
    }

    public function rockShoxSuperDeluxe()
    {
        return $this->state(function () {
            return [
                'manufacturer_id' => $this->rockShox(),
                'name' => 'Super Deluxe',
                'type' => PartType::SHOCK,
            ];
        })
            ->has(PartSettingsRecord::factory()->count(2), 'settingsRecords')
            ->has(PartSpecificationsRecord::factory(), 'specificationsRecord');
    }
}
