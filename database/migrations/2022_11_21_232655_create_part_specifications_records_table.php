<?php

use App\Enums\AirOrCoilType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('part_specifications_records', function (
            Blueprint $table
        ) {
            $table->id();
            $table->timestamps();
            $table
                ->foreignId('part_id')
                ->constrained()
                ->onDelete('cascade');
            $table->integer('lsc_max_clicks')->nullable();
            $table->integer('hsc_max_clicks')->nullable();
            $table->integer('lsr_max_clicks')->nullable();
            $table->integer('hsr_max_clicks')->nullable();
            $table->decimal('travel')->nullable();
            $table->decimal('stroke_length')->nullable();
            $table->decimal('eye_to_eye')->nullable();
            $table
                ->enum('air_or_coil', AirOrCoilType::getValues())
                ->default(AirOrCoilType::AIR);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('part_specification_records');
    }
};
