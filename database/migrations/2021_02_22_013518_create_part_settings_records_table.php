<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartSettingsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('part_settings_records', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table
                ->foreignId('part_id')
                ->constrained()
                ->onDelete('cascade');
            $table->string('nickname')->nullable();
            $table->integer('lsc_clicks')->nullable();
            $table->integer('hsc_clicks')->nullable();
            $table->integer('lsr_clicks')->nullable();
            $table->integer('hsr_clicks')->nullable();
            $table->decimal('volume_spacers')->nullable();
            $table->decimal('psi')->nullable();
            $table->decimal('sag')->nullable();
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('part_settings_records');
    }
}
