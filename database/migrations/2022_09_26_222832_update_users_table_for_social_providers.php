<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table
                ->string('email')
                ->nullable()
                ->change();
            $table
                ->string('password')
                ->nullable()
                ->change();
            $table->boolean('provisioned_by_social_provider')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table
                ->string('email')
                ->nullable(false)
                ->change();
            $table
                ->string('password')
                ->nullable(false)
                ->change();
            $table->dropColumn('provisioned_by_social_provider');
        });
    }
};
