<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('part_settings_records', function (Blueprint $table) {
            $table->integer('spring_rate')->nullable();
            $table->integer('preload_adjust')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('part_settings_records', function (Blueprint $table) {
            $table->dropColumn('spring_rate');
            $table->dropColumn('preload_adjust');
        });
    }
};
