<?php

namespace Database\Seeders;

use App\Models\Manufacturer;
use Illuminate\Database\Seeder;

class ManufacturerSeeder extends Seeder
{
    const NAMES = ['Fox', 'RockShox'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Manufacturer::factory()
            ->fox()
            ->create();
        Manufacturer::factory()
            ->rockShox()
            ->create();
    }
}
