<?php

namespace Database\Seeders;

use App\Models\Manufacturer;
use Illuminate\Database\Seeder;
use App\Models\Part;
use App\Models\User;

class PartSeeder extends Seeder
{
    public function run()
    {
        User::all()->each(function ($user) {
            Part::factory()
                ->fox36()
                ->create([
                    'user_id' => $user->id,
                ]);

            Part::factory()
                ->foxFloatX2()
                ->create([
                    'user_id' => $user->id,
                ]);

            Part::factory()
                ->rockShoxLyric()
                ->create([
                    'user_id' => $user->id,
                ]);

            Part::factory()
                ->rockShoxSuperDeluxe()
                ->create([
                    'user_id' => $user->id,
                ]);
        });
    }
}
