<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        User::create([
            'name' => $faker->name,
            'email' => 'graphql@test.com',
            'password' => bcrypt('password'),
        ]);

        for ($i = 0; $i < 5; ++$i) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('password'),
            ]);
        }
    }
}
