<?php

use App\Models\User;
use OhSeeSoftware\LaravelAssertEncrypted\Traits\AssertEncrypted;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

uses(Tests\TestCase::class)->in('Unit', 'Feature');
uses(AssertEncrypted::class)->in('Unit', 'Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function personalAccessTokenHeader()
{
    $personalAccessToken = User::factory()
        ->create()
        ->createToken('Foo bar')->plainTextToken;

    return [
        'Authorization' => "Bearer {$personalAccessToken}",
    ];
}

function signIn($email = 'peter@plush.bike', $password = 'password')
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation SignIn($email: String, $password: String) {
            signIn(input: { email: $email, password: $password }) {
                user {
                    id
                    name
                    email
                    profileImageUrl
                    createdAt
                    updatedAt
                }
                twoFactor
            }
        }
        ',
        [
            'email' => $email,
            'password' => $password,
        ]
    );
}

function signOut($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation SignOut {
            signOut {
                status
                message
            }
        }
        ',
        [],
        [],
        $headers
    );
}

function completeTwoFactorAuthenticationChallenge(
    $code = '12345',
    $recoveryCode = null
) {
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation TwoFactorAuthenticationChallenge(
            $code: String
            $recoveryCode: String
        ) {
            twoFactorAuthenticationChallenge(
                input: { code: $code, recoveryCode: $recoveryCode }
            ) {
                user {
                    id
                    name
                    email
                    profileImageUrl
                    createdAt
                    updatedAt
                }
            }
        }      
        ',
        [
            'code' => $code,
            'recoveryCode' => $recoveryCode,
        ]
    );
}

function confirmPassword($password = 'password', $headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation ConfirmPassword($password: String!) {
            confirmPassword(input: { password: $password }) {
                status
                message
            }
        }
        ',
        [
            'password' => $password,
        ],
        [],
        $headers
    );
}
