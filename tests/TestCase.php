<?php

namespace Tests;

use ImageKit\ImageKit;
use Tests\TestResponse;
use Mockery\MockInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    use MakesGraphQLRequests {
        graphQL as traitGraphQL;
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->mock(ImageKit::class, function (MockInterface $mock) {
            $mock->shouldReceive('url')->andReturnUsing(function ($options) {
                return "https://ik.imagekit.io/{$options['path']}";
            });
        });
    }

    /**
     * Create the test response instance from the given response.
     *
     * @param  \Illuminate\Http\Response  $response
     * @return \Tests\TestResponse
     */
    protected function createTestResponse($response)
    {
        return TestResponse::fromBaseResponse($response);
    }

    /**
     * Execute a query as if it was sent as a request to the server.
     *
     * @param  string  $query  The GraphQL query to send
     * @param  array<string, mixed>  $variables  The variables to include in the query
     * @param  array<string, mixed>  $extraParams  Extra parameters to add to the JSON payload
     * @param  array<string, mixed>  $headers  HTTP headers to pass to the POST request
     *
     * @return \Illuminate\Testing\TestResponse
     */
    protected function graphQL(
        string $query,
        array $variables = [],
        array $extraParams = [],
        array $headers = []
    ) {
        return $this->traitGraphQL(
            $query,
            $variables,
            $extraParams,
            array_merge(
                ['referer' => env('SANCTUM_STATEFUL_DOMAINS')],
                $headers
            )
        );
    }
}
