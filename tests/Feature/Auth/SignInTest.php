<?php

use App\Models\User;
use Mockery\MockInterface;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;

beforeEach(function () {
    $this->user = User::factory()->create([
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
        'password' => Hash::make('password'),
    ]);

    $this->userWithTwoFactorAuthentication = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'name' => 'Peter Hegman',
            'email' => 'peter2fa@plush.bike',
            'password' => Hash::make('password'),
        ]);
});

test('a user can sign in', function () {
    signIn()->assertJson([
        'data' => [
            'signIn' => [
                'user' => [
                    'id' => $this->user->id,
                    'name' => $this->user->name,
                    'email' => $this->user->email,
                    'profileImageUrl' => $this->user->profile_image_url,
                    'createdAt' => $this->user->created_at,
                    'updatedAt' => $this->user->updated_at,
                ],
            ],
        ],
    ]);

    $this->assertAuthenticatedAs($this->user, 'web');
});

test('a user can not sign in with incorrect credentials', function () {
    signIn(password: 'foo')->assertJson([
        'errors' => [
            [
                'extensions' => [],
            ],
        ],
    ]);
});

test(
    'a user with two factor authentication enabled must complete challenge before they are signed in',
    function () {
        $this->partialMock(TwoFactorAuthenticationProvider::class, function (
            MockInterface $mock
        ) {
            $mock
                ->shouldReceive('verify')
                ->once()
                ->andReturn(true);
        });

        signIn(
            email: $this->userWithTwoFactorAuthentication->email
        )->assertJson([
            'data' => [
                'signIn' => [
                    'user' => null,
                    'twoFactor' => true,
                ],
            ],
        ]);

        $this->assertGuest('web');

        completeTwoFactorAuthenticationChallenge()->assertJson([
            'data' => [
                'twoFactorAuthenticationChallenge' => [
                    'user' => [
                        'id' => $this->userWithTwoFactorAuthentication->id,
                        'name' => $this->userWithTwoFactorAuthentication->name,
                        'email' =>
                            $this->userWithTwoFactorAuthentication->email,
                        'profileImageUrl' =>
                            $this->userWithTwoFactorAuthentication
                                ->profile_image_url,
                        'createdAt' =>
                            $this->userWithTwoFactorAuthentication->created_at,
                        'updatedAt' =>
                            $this->userWithTwoFactorAuthentication->updated_at,
                    ],
                ],
            ],
        ]);

        $this->assertAuthenticatedAs(
            $this->userWithTwoFactorAuthentication,
            'web'
        );
    }
);

test('email and password fields are required', function () {
    signIn(email: '', password: '')->assertValidationErrors([
        'email' => ['Email is required.'],
        'password' => ['Password is required.'],
    ]);
});

test('email field must be a valid email', function () {
    signIn(email: 'invalid email')->assertValidationErrors([
        'email' => ['Email must be a valid email address.'],
    ]);
});
