<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'password' => Hash::make('password'),
        ]);
    $this->userWithNoPassword = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'password' => null,
        ]);
});

function twoFactorAuthenticationRecoveryCodes($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation TwoFactorAuthenticationRecoveryCodes {
            twoFactorAuthenticationRecoveryCodes {
                recoveryCodes
            }
        }                        
    ',
        [],
        [],
        $headers
    );
}

test(
    'an authenticated user can view recovery codes after confirming password',
    function () {
        $this->actingAs($this->user);

        twoFactorAuthenticationRecoveryCodes()->assertJson([
            'errors' => [
                [
                    'message' => 'Please enter your password to continue.',
                ],
            ],
        ]);

        confirmPassword();

        twoFactorAuthenticationRecoveryCodes()->assertJson([
            'data' => [
                'twoFactorAuthenticationRecoveryCodes' => [
                    'recoveryCodes' => [
                        'fake-recovery-code-1',
                        'fake-recovery-code-2',
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated user with no password can view recovery codes',
    function () {
        $this->actingAs($this->userWithNoPassword);

        twoFactorAuthenticationRecoveryCodes()->assertJson([
            'data' => [
                'twoFactorAuthenticationRecoveryCodes' => [
                    'recoveryCodes' => [
                        'fake-recovery-code-1',
                        'fake-recovery-code-2',
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated user without two factor authentication enabled can not view recovery codes',
    function () {
        $user = User::factory()->create([
            'password' => Hash::make('password'),
        ]);

        $this->actingAs($user);

        twoFactorAuthenticationRecoveryCodes();

        confirmPassword();

        twoFactorAuthenticationRecoveryCodes()->assertJson([
            'errors' => [
                [
                    'message' =>
                        'You must enable two factor authentication before recovery codes can be viewed.',
                ],
            ],
        ]);
    }
);

test(
    'a user authenticated with a personal access token can not view recovery codes',
    function () {
        twoFactorAuthenticationRecoveryCodes(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test('an unauthenticated user can not view recovery codes', function () {
    twoFactorAuthenticationRecoveryCodes()->assertUnauthenticated();
});
