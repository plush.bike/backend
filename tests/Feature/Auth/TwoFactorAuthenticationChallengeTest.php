<?php

use App\Models\User;
use Mockery\MockInterface;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;

beforeEach(function () {
    $this->user = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'email' => 'peter@plush.bike',
            'password' => Hash::make('password'),
        ]);
});

test(
    'a user can complete challenge with one time password to finish signing in',
    function () {
        $this->partialMock(TwoFactorAuthenticationProvider::class, function (
            MockInterface $mock
        ) {
            $mock
                ->shouldReceive('verify')
                ->once()
                ->andReturn(true);
        });

        signIn()->assertJson([
            'data' => [
                'signIn' => [
                    'user' => null,
                    'twoFactor' => true,
                ],
            ],
        ]);

        $this->assertGuest('web');

        completeTwoFactorAuthenticationChallenge()->assertJson([
            'data' => [
                'twoFactorAuthenticationChallenge' => [
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                        'email' => $this->user->email,
                        'profileImageUrl' => $this->user->profile_image_url,
                        'createdAt' => $this->user->created_at,
                        'updatedAt' => $this->user->updated_at,
                    ],
                ],
            ],
        ]);

        $this->assertAuthenticatedAs($this->user, 'web');
    }
);

test('a user is not signed in when one time password is invalid', function () {
    $this->partialMock(TwoFactorAuthenticationProvider::class, function (
        MockInterface $mock
    ) {
        $mock
            ->shouldReceive('verify')
            ->once()
            ->andReturn(false);
    });

    signIn()->assertJson([
        'data' => [
            'signIn' => [
                'user' => null,
                'twoFactor' => true,
            ],
        ],
    ]);

    $this->assertGuest('web');

    completeTwoFactorAuthenticationChallenge()->assertJson([
        'errors' => [
            [
                'message' => 'Invalid one time password.',
            ],
        ],
    ]);

    $this->assertGuest('web');
});

test(
    'a user can complete challenge with recovery code to finish signing in. Recovery code can only be used once.',
    function () {
        signIn()->assertJson([
            'data' => [
                'signIn' => [
                    'user' => null,
                    'twoFactor' => true,
                ],
            ],
        ]);

        $this->assertGuest('web');

        completeTwoFactorAuthenticationChallenge(
            code: null,
            recoveryCode: 'fake-recovery-code-1'
        )->assertJson([
            'data' => [
                'twoFactorAuthenticationChallenge' => [
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                        'email' => $this->user->email,
                        'profileImageUrl' => $this->user->profile_image_url,
                    ],
                ],
            ],
        ]);

        $this->assertAuthenticatedAs($this->user, 'web');

        signOut();

        $this->assertGuest('web');

        signIn();

        $this->assertGuest('web');

        completeTwoFactorAuthenticationChallenge(
            code: null,
            recoveryCode: 'fake-recovery-code-1'
        )->assertJson([
            'errors' => [
                [
                    'message' => 'Invalid recovery code.',
                ],
            ],
        ]);

        $this->assertGuest('web');
    }
);

test(
    'a user can not complete the challenge without first signing in',
    function () {
        completeTwoFactorAuthenticationChallenge()->assertJson([
            'errors' => [
                [
                    'message' =>
                        'You must sign in before completing the two factor authentication challenge.',
                ],
            ],
        ]);
    }
);
