<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()
        ->twoFactorAuthenticationUnconfirmed()
        ->create();
});

function twoFactorAuthenticationQrCode($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation TwoFactorAuthenticationQrCode {
            twoFactorAuthenticationQrCode {
                svg
            }
        }            
        ',
        [],
        [],
        $headers
    );
}

test('an authenticated user can request a QR code', function () {
    $this->actingAs($this->user);

    twoFactorAuthenticationQrCode()->assertJsonStructure([
        'data' => [
            'twoFactorAuthenticationQrCode' => ['svg'],
        ],
    ]);
});

test(
    'an authenticated user that has not yet enabled two factor authentication can not request a QR code',
    function () {
        $user = User::factory()->create();

        $this->actingAs($user);

        twoFactorAuthenticationQrCode()->assertJson([
            'errors' => [
                [
                    'message' =>
                        'You must enable two factor authentication before QR code can be generated.',
                ],
            ],
        ]);
    }
);

test(
    'an authenticated user that has already confirmed two factor authentication can not request a QR code',
    function () {
        $user = User::factory()
            ->twoFactorAuthenticationEnabled()
            ->create();

        $this->actingAs($user);

        twoFactorAuthenticationQrCode()->assertJson([
            'errors' => [
                [
                    'message' =>
                        'Two factor authentication has already been confirmed. QR code can not be generated.',
                ],
            ],
        ]);
    }
);

test(
    'a user authenticated with a personal access token can not request a QR code',
    function () {
        twoFactorAuthenticationQrCode(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test('an unauthenticated user can not request a QR code', function () {
    twoFactorAuthenticationQrCode()->assertUnauthenticated();
});
