<?php

use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()->create([
        'password' => Hash::make('password'),
    ]);
});

test('it updates session when password is correct', function () {
    $this->freezeTime(function (Carbon $time) {
        $this->actingAs($this->user);
        confirmPassword()
            ->assertJson([
                'data' => [
                    'confirmPassword' => [
                        'status' => 'SUCCESS',
                        'message' => 'Password confirmed successfully.',
                    ],
                ],
            ])
            ->assertSessionHas(
                'auth.password_confirmed_at',
                $time->getTimestamp()
            );
    });
});

test('it returns validation error when password is incorrect', function () {
    $this->actingAs($this->user);

    confirmPassword(password: 'foo')->assertValidationErrors([
        'password' => ['Password is incorrect.'],
    ]);
});

test(
    'a user authenticated with a personal access token can not confirm their password',
    function () {
        confirmPassword(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test('an unauthenticated user can not confirm their password', function () {
    confirmPassword()->assertUnauthenticated();
});
