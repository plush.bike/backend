<?php

use App\Models\User;
use Mockery\MockInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;

beforeEach(function () {
    $this->user = User::factory()
        ->twoFactorAuthenticationUnconfirmed()
        ->create([
            'password' => Hash::make('password'),
        ]);
});

function confirmTwoFactorAuthentication($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation ConfirmTwoFactorAuthentication($code: String!) {
            confirmTwoFactorAuthentication(input: { code: $code }) {
                status
                message
                recoveryCodes
            }
        }           
        ',
        [
            'code' => '12345',
        ],
        [],
        $headers
    );
}

test(
    'an authenticated user can confirm two factor authentication',
    function () {
        $this->partialMock(TwoFactorAuthenticationProvider::class, function (
            MockInterface $mock
        ) {
            $mock
                ->shouldReceive('verify')
                ->once()
                ->andReturn(true);
        });

        $this->freezeTime(function (Carbon $time) {
            $this->actingAs($this->user);

            confirmTwoFactorAuthentication()->assertJson([
                'data' => [
                    'confirmTwoFactorAuthentication' => [
                        'status' => 'SUCCESS',
                        'message' => 'Two factor authentication confirmed.',
                        'recoveryCodes' => [
                            'fake-recovery-code-1',
                            'fake-recovery-code-2',
                        ],
                    ],
                ],
            ]);

            expect($this->user->two_factor_confirmed_at->getTimestamp())->toBe(
                $time->getTimestamp()
            );
        });
    }
);

test(
    'an authenticated user can not confirm two factor authentication when one time code is incorrect',
    function () {
        $this->partialMock(TwoFactorAuthenticationProvider::class, function (
            MockInterface $mock
        ) {
            $mock
                ->shouldReceive('verify')
                ->once()
                ->andReturn(false);
        });

        $this->actingAs($this->user);

        confirmTwoFactorAuthentication()->assertValidationErrors([
            'code' => ['The provided one time password was invalid.'],
        ]);
    }
);

test(
    'an authenticated user can not confirm two factor authentication if it has already been confirmed',
    function () {
        $userWithTwoFactorAuthenticationEnabled = User::factory()
            ->twoFactorAuthenticationEnabled()
            ->create([
                'name' => 'Peter Hegman',
                'password' => Hash::make('password'),
            ]);

        $this->actingAs($userWithTwoFactorAuthenticationEnabled);

        confirmTwoFactorAuthentication()->assertJson([
            'errors' => [
                [
                    'extensions' => [],
                ],
            ],
        ]);
    }
);

test(
    'a user authenticated with a personal access token can not confirm two factor authentication',
    function () {
        confirmTwoFactorAuthentication(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test(
    'an unauthenticated user can not confirm two factor authentication',
    function () {
        confirmTwoFactorAuthentication()->assertUnauthenticated();
    }
);
