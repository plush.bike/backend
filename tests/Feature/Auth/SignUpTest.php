<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Biscolab\ReCaptcha\Facades\ReCaptcha;

function signUp(
    $name = 'Peter Hegman',
    $email = 'peter@plush.bike',
    $password = 'password',
    $passwordConfirmation = 'password',
    $shouldRecaptchaPass = true
) {
    ReCaptcha::shouldReceive('validate')
        ->once()
        ->andReturn($shouldRecaptchaPass);

    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation SignUp(
            $name: String
            $email: String
            $password: String
            $passwordConfirmation: String
        ) {
            signUp(
                input: {
                    recaptchaResponse: "fadsf78934rhkldfaif9i3o2jlkfsd"
                    name: $name
                    email: $email
                    password: $password
                    password_confirmation: $passwordConfirmation
                }
            ) {
                status
            }
        }
        ',
        [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'passwordConfirmation' => $passwordConfirmation,
        ]
    );
}

test('a user can successfully sign up', function () {
    signUp()->assertJson([
        'data' => [
            'signUp' => [
                'status' => 'SUCCESS',
            ],
        ],
    ]);

    $user = User::first();

    $this->assertEquals($user->name, 'Peter Hegman');
    $this->assertEquals($user->email, 'peter@plush.bike');
    $this->assertTrue(Hash::check('password', $user->password));
});

test('name, email, and password fields are required', function () {
    signUp(name: '', email: '', password: '')->assertValidationErrors([
        'name' => ['Name is required.'],
        'email' => ['Email is required.'],
        'password' => ['Password is required.'],
    ]);
});

test('email field must be a valid email', function () {
    signUp(email: 'invalid email')->assertValidationErrors([
        'email' => ['Email must be a valid email address.'],
    ]);
});

test('password must be at least 8 characters', function () {
    signUp(
        password: 'foo',
        passwordConfirmation: 'foo'
    )->assertValidationErrors([
        'password' => ['Password must be at least 8 characters.'],
    ]);
});

test('password and password confirmation must match', function () {
    signUp(
        password: 'foo',
        passwordConfirmation: 'bar'
    )->assertValidationErrors([
        'password' => ['Password confirmation does not match.'],
    ]);
});

test('a user can not sign up with the same email as another user', function () {
    User::factory()->create([
        'email' => 'peter@plush.bike',
    ]);

    signUp()->assertValidationErrors([
        'email' => ['Email has already been taken.'],
    ]);
});

test('a user can not sign up if the recaptcha does not pass', function () {
    signUp(shouldRecaptchaPass: false)->assertValidationErrors([
        'recaptchaResponse' => ['reCAPTCHA was invalid, please try again.'],
    ]);
});
