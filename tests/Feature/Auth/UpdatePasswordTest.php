<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()->create([
        'password' => Hash::make('password'),
    ]);
    $this->userWithNoPassword = User::factory()->create([
        'password' => null,
    ]);
});

function updatePassword(
    $currentPassword = 'password',
    $password = 'new_password',
    $passwordConfirmation = 'new_password',
    $headers = []
) {
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdatePassword(
            $currentPassword: String
            $password: String!
            $passwordConfirmation: String!
          ) {
            updatePassword(
                input: {
                    currentPassword: $currentPassword
                    password: $password
                    password_confirmation: $passwordConfirmation
                }
            ) {
                status
                message
            }
        }
        ',
        [
            'currentPassword' => $currentPassword,
            'password' => $password,
            'passwordConfirmation' => $passwordConfirmation,
        ],
        [],
        $headers
    );
}

test('an authenticated user can update their password', function () {
    $this->actingAs($this->user);

    updatePassword()->assertJson([
        'data' => [
            'updatePassword' => [
                'status' => 'SUCCESS',
                'message' => 'Your password has been updated.',
            ],
        ],
    ]);
    $this->assertTrue(Hash::check('new_password', $this->user->password));
});

test(
    'a user can not update their password if current password is incorrect',
    function () {
        $this->actingAs($this->user);

        updatePassword('foo')->assertValidationErrors([
            'currentPassword' => ['Current password is incorrect.'],
        ]);
    }
);

test('password must be at least 8 characters', function () {
    updatePassword(
        password: 'foo',
        passwordConfirmation: 'foo'
    )->assertValidationErrors([
        'password' => ['Password must be at least 8 characters.'],
    ]);
});

test('password and password confirmation must match', function () {
    updatePassword(
        password: 'new_password',
        passwordConfirmation: 'new_password_foo'
    )->assertValidationErrors([
        'password' => ['Password confirmation does not match.'],
    ]);
});

test(
    'a user authenticated with a personal access token can not update their password',
    function () {
        updatePassword(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test('a user with no password set can set their password', function () {
    $this->actingAs($this->userWithNoPassword);

    updatePassword(currentPassword: null)->assertJson([
        'data' => [
            'updatePassword' => [
                'status' => 'SUCCESS',
                'message' => 'Your password has been updated.',
            ],
        ],
    ]);
    $this->assertTrue(
        Hash::check('new_password', $this->userWithNoPassword->password)
    );
});

test('an unauthenticated user can not update their password', function () {
    updatePassword()->assertUnauthenticated();
});
