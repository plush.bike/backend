<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
});

test('an authenticated user can sign out', function () {
    $this->actingAs($this->user);

    signOut()->assertJson([
        'data' => [
            'signOut' => [
                'status' => 'SUCCESS',
                'message' => 'Your session has been terminated.',
            ],
        ],
    ]);

    $this->assertGuest('web');
});

test(
    'a user authenticated with a personal access token can not sign out',
    function () {
        signOut(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test('an unauthenticated user can not sign out', function () {
    signOut()->assertUnauthenticated();
});
