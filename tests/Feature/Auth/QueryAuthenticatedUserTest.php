<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create([
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
    ]);
});

function authenticatedUser()
{
    return test()->graphQL(/** @lang GraphQL */ '
        query AuthenticatedUser {
            authenticatedUser {
                name
            }
        }
        ');
}

test('an authenticated user can query the authenticated user', function () {
    $this->actingAs($this->user);

    authenticatedUser()->assertJson([
        'data' => [
            'authenticatedUser' => [
                'name' => $this->user->name,
            ],
        ],
    ]);
});

test(
    'an unauthenticated user can not query the authenticated user',
    function () {
        authenticatedUser()->assertUnauthenticated();
    }
);
