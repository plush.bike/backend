<?php

use App\Enums\SocialProviderCallbackType;
use App\Enums\SocialProviderType;
use App\Models\SocialProvider;
use App\Models\User;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User as SocialiteUser;

beforeEach(function () {
    $this->token = 'fake token';
    $this->refreshToken = 'fake refresh token';

    $user = [
        'id' => 12345,
        'nickname' => 'Pete',
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
        'avatar' => null,
    ];
    $this->socialiteUser = (new SocialiteUser())
        ->setRaw($user)
        ->map([
            'id' => $user['id'],
            'nickname' => $user['nickname'],
            'name' => $user['name'],
            'email' => $user['email'],
            'avatar' => $user['avatar'],
        ])
        ->setToken($this->token)
        ->setRefreshToken($this->refreshToken);
});

function socialProviderCallback(
    $code = '12345',
    $provider = SocialProviderType::STRAVA,
    $success = true
) {
    if ($success) {
        Socialite::shouldReceive('driver->stateless->user')->andReturn(
            test()->socialiteUser
        );
    } else {
        Socialite::shouldReceive('driver->stateless->user')->andThrow(
            new Exception()
        );
    }

    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation SocialProviderCallback(
            $code: String!
            $provider: SocialProviderType!
        ) {
            socialProviderCallback(input: { code: $code, provider: $provider }) {
                user {
                    id
                    name
                    email
                    profileImageUrl
                    hasEnabledTwoFactorAuthentication
                    hasPasswordSet
                    createdAt
                    updatedAt
                }
                twoFactor
                callbackType
            }
        }
        ',
        [
            'code' => $code,
            'provider' => $provider,
        ]
    );
}

test(
    'when a user has not sign up and Socialite callback is successful a user is created and authenticated',
    function () {
        $response = socialProviderCallback();
        $expectedUser = SocialProvider::where(
            'provider_user_id',
            $this->socialiteUser->getId()
        )->first()->user;

        $response->assertJson([
            'data' => [
                'socialProviderCallback' => [
                    'user' => [
                        'id' => strval($expectedUser->id),
                        'name' => $expectedUser->name,
                        'email' => $expectedUser->email,
                    ],
                    'twoFactor' => false,
                    'callbackType' => SocialProviderCallbackType::SIGN_IN,
                ],
            ],
        ]);
        $this->assertAuthenticatedAs($expectedUser, 'web');
        expect($expectedUser->provisioned_by_social_provider)->toBe(true);
    }
);

test(
    'when a user has signed up and Socialite callback is successful a user is authenticated',
    function () {
        $signedUpUser = User::factory()->create([
            'name' => 'Peter Hegman',
            'email' => 'peter@plush.bike',
            'password' => null,
        ]);
        SocialProvider::create([
            'provider_user_id' => $this->socialiteUser->getId(),
            'user_id' => $signedUpUser->id,
            'provider' => SocialProviderType::STRAVA,
        ]);

        socialProviderCallback();

        $this->assertAuthenticatedAs($signedUpUser, 'web');
    }
);

test(
    'when a user is not signed in and Socialite callback is not successful',
    function () {
        $formattedProviderTitle = ucwords(
            strtolower(SocialProviderType::STRAVA)
        );

        socialProviderCallback(success: false)->assertJson([
            'errors' => [
                [
                    'message' => "Could not communicate with {$formattedProviderTitle}.",
                    'extensions' => [
                        'callbackType' => SocialProviderCallbackType::SIGN_IN,
                    ],
                ],
            ],
        ]);
    }
);

test(
    'when user is not signed in and email has already been taken',
    function () {
        User::factory()->create([
            'name' => 'Peter Hegman',
            'email' => 'peter@plush.bike',
            'password' => null,
        ]);

        socialProviderCallback()->assertJson([
            'errors' => [
                [
                    'message' => 'An account with this email already exists.',
                    'extensions' => [
                        'callbackType' => SocialProviderCallbackType::SIGN_IN,
                    ],
                ],
            ],
        ]);
    }
);

test(
    'when a user is signed in and Socialite callback is successful the provider is connected',
    function () {
        $user = User::factory()->create([
            'name' => 'Peter Hegman',
            'email' => 'peter@plush.bike',
            'password' => null,
        ]);

        $this->actingAs($user);

        $response = socialProviderCallback();

        $response->assertJson([
            'data' => [
                'socialProviderCallback' => [
                    'user' => null,
                    'twoFactor' => null,
                    'callbackType' => SocialProviderCallbackType::CONNECT,
                ],
            ],
        ]);
        $this->assertModelExists(
            SocialProvider::where('user_id', $user->id)->first()
        );
    }
);

test(
    'when a user is signed in and Socialite callback is not successful',
    function () {
        $user = User::factory()->create([
            'name' => 'Peter Hegman',
            'email' => 'peter@plush.bike',
            'password' => null,
        ]);

        $this->actingAs($user);

        $formattedProviderTitle = ucwords(
            strtolower(SocialProviderType::STRAVA)
        );

        socialProviderCallback(success: false)->assertJson([
            'errors' => [
                [
                    'message' => "Could not communicate with {$formattedProviderTitle}.",
                    'extensions' => [
                        'callbackType' => SocialProviderCallbackType::CONNECT,
                    ],
                ],
            ],
        ]);
    }
);

test('when a user is signed in and provider already exists', function () {
    $user = User::factory()->create([
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
        'password' => null,
    ]);

    SocialProvider::factory()->create([
        'user_id' => $user,
        'provider' => SocialProviderType::STRAVA,
        'provider_user_id' => '12345',
    ]);

    $this->actingAs($user);

    $formattedProviderTitle = ucwords(strtolower(SocialProviderType::STRAVA));

    socialProviderCallback()->assertJson([
        'errors' => [
            [
                'message' => "Could not connect {$formattedProviderTitle} to your account.",
                'extensions' => [
                    'callbackType' => SocialProviderCallbackType::CONNECT,
                ],
            ],
        ],
    ]);
});
