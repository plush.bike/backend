<?php

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

beforeEach(function () {
    Storage::fake();

    $this->user = User::factory()->create([
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
        'profile_image_src' => null,
    ]);
    $this->user2 = User::factory()->create([
        'name' => 'Greg Minnaar',
        'email' => 'greg@santacruzbicycles.com',
        'profile_image_src' => null,
    ]);
});

$defaultProfileImage = UploadedFile::fake()->image('profile-image.jpg');

function updateAuthenticatedUser(
    $email = 'peter.updated@plush.bike',
    $profileImage = null
) {
    $operations = [
        'query' => /** @lang GraphQL */ '
            mutation UpdateAuthenticatedUser(
                $name: String
                $email: String
                $profileImage: Upload
            ) {
                updateAuthenticatedUser(
                    input: { name: $name, email: $email, profileImage: $profileImage }
                ) {
                    id
                    name
                    email
                    profileImageUrl
                    createdAt
                    updatedAt
                }
            }
        ',
        'variables' => [
            'name' => 'Peter Hegman Updated',
            'email' => $email,
            'profileImage' => null,
        ],
    ];

    return test()->multipartGraphQL(
        $operations,
        isset($profileImage)
            ? [
                '0' => ['variables.profileImage'],
            ]
            : [],
        isset($profileImage)
            ? [
                '0' => $profileImage,
            ]
            : []
    );
}

test('an authenticated user can update their profile', function () use (
    $defaultProfileImage
) {
    $this->actingAs($this->user);

    updateAuthenticatedUser(profileImage: $defaultProfileImage)->assertJson([
        'data' => [
            'updateAuthenticatedUser' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'email' => $this->user->email,
                'profileImageUrl' => "https://ik.imagekit.io/{$this->user->profile_image_src}",
            ],
        ],
    ]);
});

test(
    'an authenticated user can not update their email to one that is already in use',
    function () {
        $this->actingAs($this->user);

        updateAuthenticatedUser(
            email: 'greg@santacruzbicycles.com'
        )->assertValidationErrors([
            'email' => ['Email has already been taken.'],
        ]);
    }
);

test('email field must be a valid email', function () {
    $this->actingAs($this->user);

    updateAuthenticatedUser(email: 'invalid email')->assertValidationErrors([
        'email' => ['Email must be a valid email address.'],
    ]);
});

test('profile image must be less than 10MB', function () {
    $this->actingAs($this->user);

    updateAuthenticatedUser(
        profileImage: UploadedFile::fake()
            ->image('profile-image.jpg')
            ->size(11000)
    )->assertValidationErrors([
        'profileImage' => ['File must be less than 10MB.'],
    ]);
});

test('profile image must be an image', function () {
    $this->actingAs($this->user);

    updateAuthenticatedUser(
        profileImage: UploadedFile::fake()->create('profile-image.pdf')
    )->assertValidationErrors([
        'profileImage' => [
            'File must be an image (jpg, jpeg, png, bmp, gif, svg, or webp).',
        ],
    ]);
});

test('an unauthenticated user can not update their profile', function () {
    updateAuthenticatedUser()->assertUnauthenticated();
});
