<?php

use App\Models\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ResetPassword;

beforeEach(function () {
    Notification::fake();

    $this->user = User::factory()->create([
        'email' => 'peter@plush.bike',
    ]);
});

function forgotPassword($email = 'peter@plush.bike')
{
    return test()->graphQL(
        /** @lang GraphQL */ '
            mutation ForgotPassword($email: String) {
                forgotPassword(input: { email: $email }) {
                    status
                    message
                }
            }
            ',
        [
            'email' => $email,
        ]
    );
}

test('a user can send password reset email', function () {
    forgotPassword()->assertJson([
        'data' => [
            'forgotPassword' => [
                'status' => 'SUCCESS',
                'message' => 'Reset password email sent successfully.',
            ],
        ],
    ]);

    Notification::assertSentTo([$this->user], ResetPassword::class);
});

test(
    'a user can not send password reset email if email does not exist',
    function () {
        forgotPassword(email: 'does.not.exist@plush.bike')->assertJson([
            'errors' => [
                [
                    'extensions' => [],
                ],
            ],
        ]);
    }
);

test('email field is required', function () {
    forgotPassword(email: '')->assertValidationErrors([
        'email' => ['Email is required.'],
    ]);
});

test('email field must be a valid email', function () {
    forgotPassword(email: 'invalid email')->assertValidationErrors([
        'email' => ['Email must be a valid email address.'],
    ]);
});
