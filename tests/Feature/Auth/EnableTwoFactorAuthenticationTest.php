<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()->create([
        'password' => Hash::make('password'),
    ]);
});

function enableTwoFactorAuthentication($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation EnableTwoFactorAuthentication {
            enableTwoFactorAuthentication {
                status
                message
            }
        }        
        ',
        [],
        [],
        $headers
    );
}

test('an authenticated user can enable two factor authentication', function () {
    $this->actingAs($this->user);

    enableTwoFactorAuthentication()->assertJson([
        'data' => [
            'enableTwoFactorAuthentication' => [
                'status' => 'SUCCESS',
                'message' => 'Two factor authentication enabled.',
            ],
        ],
    ]);
});

test(
    'a user authenticated with a personal access token can not enable two factor authentication',
    function () {
        enableTwoFactorAuthentication(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test(
    'an unauthenticated user can not enable two factor authentication',
    function () {
        enableTwoFactorAuthentication()->assertUnauthenticated();
    }
);
