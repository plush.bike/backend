<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    $this->user = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'password' => Hash::make('password'),
        ]);
    $this->userWithNoPassword = User::factory()
        ->twoFactorAuthenticationEnabled()
        ->create([
            'password' => null,
        ]);
});

function disableTwoFactorAuthentication($headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation DisableTwoFactorAuthentication {
            disableTwoFactorAuthentication {
                status
                message
            }
        }                  
    ',
        [],
        [],
        $headers
    );
}

test(
    'an authenticated user can disable two factor authentication after confirming password',
    function () {
        $this->actingAs($this->user);

        disableTwoFactorAuthentication()->assertJson([
            'errors' => [
                [
                    'message' => 'Please enter your password to continue.',
                ],
            ],
        ]);

        confirmPassword();

        disableTwoFactorAuthentication()->assertJson([
            'data' => [
                'disableTwoFactorAuthentication' => [
                    'status' => 'SUCCESS',
                    'message' => 'Two factor authentication disabled.',
                ],
            ],
        ]);

        expect($this->user->two_factor_secret)->toBeNull();
        expect($this->user->two_factor_recovery_codes)->toBeNull();
        expect($this->user->two_factor_confirmed_at)->toBeNull();
    }
);

test(
    'an authenticated user with no password can disable two factor authentication',
    function () {
        $this->actingAs($this->userWithNoPassword);

        disableTwoFactorAuthentication()->assertJson([
            'data' => [
                'disableTwoFactorAuthentication' => [
                    'status' => 'SUCCESS',
                    'message' => 'Two factor authentication disabled.',
                ],
            ],
        ]);

        expect($this->userWithNoPassword->two_factor_secret)->toBeNull();
        expect(
            $this->userWithNoPassword->two_factor_recovery_codes
        )->toBeNull();
        expect($this->userWithNoPassword->two_factor_confirmed_at)->toBeNull();
    }
);

test(
    'a user authenticated with a personal access token can not disable two factor authentication',
    function () {
        disableTwoFactorAuthentication(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test(
    'an unauthenticated user can not disable two factor authentication',
    function () {
        disableTwoFactorAuthentication()->assertUnauthenticated();
    }
);
