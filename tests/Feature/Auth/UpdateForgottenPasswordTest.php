<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

beforeEach(function () {
    $this->user = User::factory()->create([
        'name' => 'Peter Hegman',
        'email' => 'peter@plush.bike',
        'password' => Hash::make('password'),
    ]);

    $this->token = app('auth.password.broker')->createToken($this->user);
});

function updateForgottenPassword(
    $email = 'peter@plush.bike',
    $token = null,
    $password = 'new_password',
    $passwordConfirmation = 'new_password'
) {
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdateForgottenPassword(
            $email: String
            $token: String
            $password: String
            $passwordConfirmation: String
        ) {
            updateForgottenPassword(
                input: {
                    email: $email
                    token: $token
                    password: $password
                    password_confirmation: $passwordConfirmation
                }
            ) {
                status
                message
            }
        }
        ',
        [
            'email' => $email,
            'token' => $token !== null ? $token : test()->token,
            'password' => $password,
            'passwordConfirmation' => $passwordConfirmation,
        ]
    );
}

test('a user can update their password', function () {
    updateForgottenPassword()->assertJson([
        'data' => [
            'updateForgottenPassword' => [
                'status' => 'SUCCESS',
                'message' => 'Password successfully updated.',
            ],
        ],
    ]);
});

test('email, token, and password fields are required', function () {
    updateForgottenPassword(
        email: '',
        token: '',
        password: '',
        passwordConfirmation: ''
    )->assertValidationErrors([
        'email' => ['Email is required.'],
        'token' => ['Token is required.'],
        'password' => ['Password is required.'],
    ]);
});

test('email field must be a valid email', function () {
    updateForgottenPassword(email: 'invalid email')->assertValidationErrors([
        'email' => ['Email must be a valid email address.'],
    ]);
});

test('password must be at least 8 characters', function () {
    updateForgottenPassword(
        password: 'foo',
        passwordConfirmation: 'foo'
    )->assertValidationErrors([
        'password' => ['Password must be at least 8 characters.'],
    ]);
});

test('password and password confirmation must match', function () {
    updateForgottenPassword(
        password: 'foo',
        passwordConfirmation: 'bar'
    )->assertValidationErrors([
        'password' => ['Password confirmation does not match.'],
    ]);
});

test('a user can not update their password if token is invalid', function () {
    updateForgottenPassword(token: 'invalid_token')->assertValidationErrors([
        'token' => [Password::INVALID_TOKEN],
    ]);
});
