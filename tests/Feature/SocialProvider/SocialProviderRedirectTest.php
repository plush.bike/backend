<?php

use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\Facades\Socialite;

beforeEach(function () {
    $this->redirectUrl = 'https://strava.com/oauth2/sign-in';
    Socialite::shouldReceive('driver->stateless->redirect')->andReturn(
        new RedirectResponse($this->redirectUrl)
    );
});

test('a valid social provider type is redirected to the provider', function () {
    $this->get('/social-provider/strava/redirect')->assertRedirect(
        $this->redirectUrl
    );
});

test('an invalid social provider returns a 404', function () {
    $this->get('/social-provider/foobar/redirect')->assertNotFound();
});
