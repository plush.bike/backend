<?php

use App\Enums\SocialProviderType;
use App\Models\SocialProvider;
use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->socialProvider1 = SocialProvider::factory()->create([
        'user_id' => $this->user,
        'provider' => SocialProviderType::STRAVA,
        'provider_user_id' => '12345',
        'token' => 'fake-token',
        'refresh_token' => 'refresh-token',
    ]);

    $this->socialProvider2 = SocialProvider::factory()->create([
        'user_id' => $this->user,
        'provider' => SocialProviderType::GOOGLE,
        'provider_user_id' => '12346',
        'token' => 'fake-token',
        'refresh_token' => 'refresh-token',
    ]);
});

function socialProviders()
{
    return test()->graphQL(/** @lang GraphQL */ '
        query SocialProviders {
            socialProviders {
                id
                provider
            }
        }             
        ');
}

test(
    'an authenticated user can query their connected social providers',
    function () {
        $this->actingAs($this->user);

        socialProviders()->assertJson([
            'data' => [
                'socialProviders' => [
                    $this->socialProvider1->only(['id', 'provider']),
                    $this->socialProvider2->only(['id', 'provider']),
                ],
            ],
        ]);
    }
);

test(
    'an unauthenticated user can not query connected social providers',
    function () {
        socialProviders()->assertUnauthenticated();
    }
);
