<?php

use App\Enums\SocialProviderType;
use App\Models\SocialProvider;
use App\Models\User;

beforeEach(function () {
    $this->user1 = User::factory()->create();
    $this->user2 = User::factory()->create();

    $this->socialProvider1 = SocialProvider::factory()->create([
        'user_id' => $this->user1,
        'provider' => SocialProviderType::STRAVA,
    ]);
    $this->socialProvider2 = SocialProvider::factory()->create([
        'user_id' => $this->user2,
        'provider' => SocialProviderType::GOOGLE,
    ]);
});

function deleteSocialProvider($id)
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation DeleteSocialProvider($id: ID!) {
            deleteSocialProvider(input: { id: $id }) {
                id
                provider
            }
        }                   
        ',
        [
            'id' => $id,
        ]
    );
}

test(
    'an authenticated user can delete a social provider they are the owner of',
    function () {
        $this->actingAs($this->user1);

        deleteSocialProvider($this->socialProvider1->id)->assertJson([
            'data' => [
                'deleteSocialProvider' => $this->socialProvider1->only([
                    'id',
                    'provider',
                ]),
            ],
        ]);
        $this->assertModelMissing($this->socialProvider1);
    }
);

test(
    'an authenticated user can not delete a social provider they are not the owner of',
    function () {
        $this->actingAs($this->user2);

        deleteSocialProvider(
            $this->socialProvider1->id
        )->assertAuthorizationError();
        $this->assertModelExists($this->socialProvider1);
    }
);

test(
    'a user without a password set can not delete a social provider if they only have one connected',
    function () {
        $this->user1->update([
            'password' => null,
        ]);

        $this->actingAs($this->user1);

        deleteSocialProvider(
            $this->socialProvider1->id
        )->assertAuthorizationError();
        $this->assertModelExists($this->socialProvider1);
    }
);

test(
    'a user without a password set can delete a social provider if they have another one connected',
    function () {
        $this->user1->update([
            'password' => null,
        ]);

        SocialProvider::factory()->create([
            'user_id' => $this->user1,
            'provider' => SocialProviderType::GOOGLE,
            'provider_user_id' => '123456',
        ]);

        $this->actingAs($this->user1);

        deleteSocialProvider($this->socialProvider1->id)->assertJson([
            'data' => [
                'deleteSocialProvider' => $this->socialProvider1->only([
                    'id',
                    'provider',
                ]),
            ],
        ]);
        $this->assertModelMissing($this->socialProvider1);
    }
);
