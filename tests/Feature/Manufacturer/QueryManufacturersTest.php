<?php

use App\Models\User;
use App\Models\Manufacturer;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->fox = Manufacturer::factory()
        ->fox()
        ->create();
    $this->rockShox = Manufacturer::factory()
        ->rockShox()
        ->create();
});

function manufacturers()
{
    return test()->graphQL(/** @lang GraphQL */ '
        query Manufacturers($first: Int = 25) {
            manufacturers(first: $first) {
                data {
                    id
                    name
                }
                paginatorInfo {
                    lastItem
                    total
                }
            }
        }
        ');
}

test('an authenticated user can query manufacturers', function () {
    $this->actingAs($this->user);

    manufacturers()->assertJson([
        'data' => [
            'manufacturers' => [
                'data' => [
                    [
                        'id' => $this->fox->id,
                        'name' => $this->fox->name,
                    ],
                    [
                        'id' => $this->rockShox->id,
                        'name' => $this->rockShox->name,
                    ],
                ],
                'paginatorInfo' => [
                    'lastItem' => 2,
                    'total' => 2,
                ],
            ],
        ],
    ]);
});

test('an unauthenticated user can not query manufacturers', function () {
    manufacturers()->assertUnauthenticated();
});
