<?php

use App\Models\Part;
use App\Models\User;
use App\Models\PartImage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

beforeEach(function () {
    Storage::fake();

    $this->user = User::factory()->create();
    $this->user2 = User::factory()->create();
    $this->rockShoxLyric = Part::factory()
        ->rockshoxLyric()
        ->create([
            'user_id' => $this->user->id,
        ]);
    $this->partImage1 = PartImage::factory()->create([
        'src' => Storage::putFile(
            'parts/1',
            UploadedFile::fake()->image('part-image1.jpg')
        ),
        'part_id' => $this->rockShoxLyric->id,
    ]);
    $this->partImage2 = PartImage::factory()->create([
        'src' => Storage::putFile(
            'parts/1',
            UploadedFile::fake()->image('part-image2.jpg')
        ),
        'part_id' => $this->rockShoxLyric->id,
    ]);
});

function deletePart()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation DeletePart($partId: ID!) {
            deletePart(input: { id: $partId }) {
                id
            }
        }
        ',
        [
            'partId' => test()->rockShoxLyric->id,
        ]
    );
}

test('an authenticated owner of the part can delete the part', function () {
    $this->actingAs($this->user);

    $expectedPartId = $this->rockShoxLyric->id;
    deletePart()->assertJson([
        'data' => [
            'deletePart' => [
                'id' => $expectedPartId,
            ],
        ],
    ]);
});

test('part images are deleted when part is deleted', function () {
    $this->actingAs($this->user);

    $partImage1Src = $this->partImage1->src;
    $partImage2Src = $this->partImage2->src;

    Storage::disk()->assertExists([$partImage1Src, $partImage2Src]);

    $expectedPartId = $this->rockShoxLyric->id;
    deletePart()->assertJson([
        'data' => [
            'deletePart' => [
                'id' => $expectedPartId,
            ],
        ],
    ]);

    Storage::disk()->assertMissing([$partImage1Src, $partImage2Src]);
    $this->assertNull(PartImage::where('src', $partImage1Src)->first());
    $this->assertNull(PartImage::where('src', $partImage2Src)->first());
});

test('an unauthorized user can not update the part', function () {
    $this->actingAs($this->user2);

    deletePart()->assertAuthorizationError();
});

test('an unauthenticated user can not update a part', function () {
    deletePart()->assertUnauthenticated();
});
