<?php

use App\Models\Part;
use App\Models\User;
use App\Models\Manufacturer;

beforeEach(function () {
    $this->manufacturer = Manufacturer::factory()
        ->rockShox()
        ->create();
    $this->user = User::factory()->create();
});

function createPart($name = 'Lyric', $type = 'FORK', $manufacturerId = '')
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation CreatePart($name: String, $type: PartType, $manufacturerId: ID!) {
            createPart(
                input: {
                    name: $name
                    manufacturer: { connect: $manufacturerId }
                    type: $type
                }
            ) {
                id
                name
                manufacturer {
                    id
                }
                type
                user {
                    id
                }
                createdAt
                updatedAt
            }
        }
        ',
        [
            'name' => $name,
            'type' => $type,
            'manufacturerId' =>
                $manufacturerId === ''
                    ? test()->manufacturer->id
                    : $manufacturerId,
        ]
    );
}

function createPartWithPartImages()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation CreatePart($manufacturerId: ID!) {
            createPart(
                input: {
                    name: "Lyric"
                    manufacturer: { connect: $manufacturerId }
                    type: FORK
                    images: {
                        create: [
                            { src: "parts/1/tP9rncM5DmCq4ybR3i9UHpiL9lmMpQKKnYzERJ6a.png" }
                            { src: "parts/1/V2UAGLjCDN7Dp0x8ZeaYrPnBBy5Kl4MErMsOIPGl.png" }
                        ]
                    }
                }
            ) {
                images {
                    url
                }
            }
        }
        ',
        [
            'manufacturerId' => test()->manufacturer->id,
        ]
    );
}

function createPartWithSettingsRecords()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation CreatePart($manufacturerId: ID!) {
            createPart(
                input: {
                    name: "Lyric"
                    manufacturer: { connect: $manufacturerId }
                    type: FORK
                    settingsRecords: {
                        create: [
                            { lscClicks: 10, hscClicks: 12, psi: 90 }
                            { lscClicks: 11, hscClicks: 10, psi: 92 }
                        ]
                    }
                }
            ) {
                settingsRecords {
                    lscClicks
                    hscClicks
                    psi
                }
            }
        }
        ',
        [
            'manufacturerId' => test()->manufacturer->id,
        ]
    );
}

function createPartWithSpecificationsRecord()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation CreatePart($manufacturerId: ID!) {
            createPart(
                input: {
                    name: "Lyric"
                    manufacturer: { connect: $manufacturerId }
                    type: FORK
                    specificationsRecord: {
                        create: {
                            lscMaxClicks: 12,
                            hscMaxClicks: 12,
                            lsrMaxClicks: 12,
                            hsrMaxClicks: 12,
                            travel: 150,
                            strokeLength: 65,
                            eyeToEye: 205,
                            airOrCoil: AIR
                        }
                    }
                }
            ) {
                specificationsRecord {
                    lscMaxClicks
                    hscMaxClicks
                    lsrMaxClicks
                    hsrMaxClicks
                    travel
                    strokeLength
                    eyeToEye
                    airOrCoil
                }
            }
        }
        ',
        [
            'manufacturerId' => test()->manufacturer->id,
        ]
    );
}

test('name is required', function () {
    $this->actingAs($this->user);

    createPart(name: '')->assertValidationErrors([
        'name' => ['Name is required.'],
    ]);
});

test('manufacturer is required', function () {
    $this->actingAs($this->user);

    createPart(manufacturerId: null)->assertGraphQLErrorMessage(
        'Variable "$manufacturerId" of non-null type "ID!" must not be null.'
    );
});

test('type must be a valid part type', function () {
    $this->actingAs($this->user);

    createPart(type: 'INVALID_TYPE')->assertGraphQLErrorMessage(
        'Variable "$type" got invalid value "INVALID_TYPE"; Value "INVALID_TYPE" does not exist in "PartType" enum.'
    );
});

test(
    'an authenticated user can create a part with required fields',
    function () {
        $this->actingAs($this->user);

        createPart()
            ->assertJson([
                'data' => [
                    'createPart' => [
                        'id' => Part::first()->id,
                        'name' => 'Lyric',
                        'manufacturer' => [
                            'id' => $this->manufacturer->id,
                        ],
                        'type' => 'FORK',
                        'user' => [
                            'id' => $this->user->id,
                        ],
                    ],
                ],
            ])
            ->assertHasTimestamps('createPart');
    }
);

test('an authenticated user can create a part with images', function () {
    $this->actingAs($this->user);

    createPartWithPartImages()->assertJson([
        'data' => [
            'createPart' => [
                'images' => [
                    [
                        'url' =>
                            'https://ik.imagekit.io/parts/1/tP9rncM5DmCq4ybR3i9UHpiL9lmMpQKKnYzERJ6a.png',
                    ],
                    [
                        'url' =>
                            'https://ik.imagekit.io/parts/1/V2UAGLjCDN7Dp0x8ZeaYrPnBBy5Kl4MErMsOIPGl.png',
                    ],
                ],
            ],
        ],
    ]);
});

test(
    'an authenticated user can create a part with settings records',
    function () {
        $this->actingAs($this->user);

        createPartWithSettingsRecords()->assertJson([
            'data' => [
                'createPart' => [
                    'settingsRecords' => [
                        [
                            'lscClicks' => 10,
                            'hscClicks' => 12,
                            'psi' => 90,
                        ],
                        [
                            'lscClicks' => 11,
                            'hscClicks' => 10,
                            'psi' => 92,
                        ],
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated user can create a part and specifications record',
    function () {
        $this->actingAs($this->user);

        createPartWithSpecificationsRecord()->assertJson([
            'data' => [
                'createPart' => [
                    'specificationsRecord' => [
                        'lscMaxClicks' => 12,
                        'hscMaxClicks' => 12,
                        'lsrMaxClicks' => 12,
                        'hsrMaxClicks' => 12,
                        'travel' => 150,
                        'strokeLength' => 65,
                        'eyeToEye' => 205,
                        'airOrCoil' => 'AIR',
                    ],
                ],
            ],
        ]);
    }
);

test('an unauthenticated user can not create a part', function () {
    createPart()->assertUnauthenticated();
});
