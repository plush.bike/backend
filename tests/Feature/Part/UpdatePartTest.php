<?php

use App\Models\Part;
use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->user2 = User::factory()->create();
    $this->rockShoxLyric = Part::factory()
        ->rockshoxLyric()
        ->create([
            'user_id' => $this->user->id,
        ]);
    $this->fox36 = Part::factory()
        ->fox36()
        ->create([
            'user_id' => $this->user->id,
        ]);
});

function updatePart()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdatePart($partId: ID!) {
            updatePart(input: { id: $partId, name: "Lyric Select" }) {
                name
                createdAt
                updatedAt
            }
        }
        ',
        [
            'partId' => test()->rockShoxLyric->id,
        ]
    );
}

function updateActiveSettingsRecord(int $activeSettingsRecord)
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdateActiveSettingsRecord($partId: ID!, $activeSettingsRecord: ID!) {
            updatePart(
                input: {
                    id: $partId
                    activeSettingsRecord: { connect: $activeSettingsRecord }
                }
            ) {
                activeSettingsRecord {
                    id
                }
            }
        }            
        ',
        [
            'partId' => test()->rockShoxLyric->id,
            'activeSettingsRecord' => $activeSettingsRecord,
        ]
    );
}

function updatePartAndSettingsRecords()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdateActiveSettingsRecord($partId: ID!, $settingsRecords: UpdateSettingsRecordsHasMany) {
            updatePart(
                input: {
                    id: $partId
                    settingsRecords: $settingsRecords
                }
            ) {
                settingsRecords {
                    nickname
                }
            }
        }            
        ',
        [
            'partId' => test()->rockShoxLyric->id,
            'settingsRecords' => [
                'create' => [['nickname' => 'new settings record']],
                'update' => [
                    [
                        'id' => test()->rockShoxLyric->settingsRecords->get(0)
                            ->id,
                        'nickname' => 'updated settings record',
                    ],
                ],
                'delete' => [
                    test()->rockShoxLyric->settingsRecords->get(1)->id,
                ],
            ],
        ]
    );
}

function updatePartWithSpecificationsRecord()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation UpdatePart($partId: ID!, $specificationsRecord: UpdateSpecificationsRecordHasOne) {
            updatePart(
                input: {
                    id: $partId
                    specificationsRecord: $specificationsRecord
                }
            ) {
                specificationsRecord {
                    lscMaxClicks
                    hscMaxClicks
                    lsrMaxClicks
                    hsrMaxClicks
                    travel
                    strokeLength
                    eyeToEye
                    airOrCoil
                }
            }
        }            
        ',
        [
            'partId' => test()->rockShoxLyric->id,
            'specificationsRecord' => [
                'update' => [
                    'id' => test()->rockShoxLyric->specificationsRecord->id,
                    'lscMaxClicks' => 12,
                    'hscMaxClicks' => 12,
                    'lsrMaxClicks' => 12,
                    'hsrMaxClicks' => 12,
                    'travel' => 150,
                    'strokeLength' => 65,
                    'eyeToEye' => 205,
                    'airOrCoil' => 'COIL',
                ],
            ],
        ]
    );
}

test('an authenticated owner of the part can update the part', function () {
    $this->actingAs($this->user);

    updatePart()
        ->assertJson([
            'data' => [
                'updatePart' => [
                    'name' => 'Lyric Select',
                ],
            ],
        ])
        ->assertHasTimestamps('updatePart');
});

test(
    'an authenticated owner of the part can update the part and the settings records',
    function () {
        $this->actingAs($this->user);

        updatePartAndSettingsRecords()->assertJson([
            'data' => [
                'updatePart' => [
                    'settingsRecords' => [
                        [
                            'nickname' => 'updated settings record',
                        ],
                        [
                            'nickname' => 'new settings record',
                        ],
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated user can update a part and specifications record',
    function () {
        $this->actingAs($this->user);

        updatePartWithSpecificationsRecord()->assertJson([
            'data' => [
                'updatePart' => [
                    'specificationsRecord' => [
                        'lscMaxClicks' => 12,
                        'hscMaxClicks' => 12,
                        'lsrMaxClicks' => 12,
                        'hsrMaxClicks' => 12,
                        'travel' => 150,
                        'strokeLength' => 65,
                        'eyeToEye' => 205,
                        'airOrCoil' => 'COIL',
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated owner of the part can update the active settings record to a settings record that is connected to the part',
    function () {
        $this->actingAs($this->user);

        $activeSettingsRecordId = $this->rockShoxLyric->settingsRecords->get(0)
            ->id;

        $this->assertNull($this->rockShoxLyric->activeSettingsRecord);
        updateActiveSettingsRecord($activeSettingsRecordId)->assertJson([
            'data' => [
                'updatePart' => [
                    'activeSettingsRecord' => [
                        'id' => $activeSettingsRecordId,
                    ],
                ],
            ],
        ]);
    }
);

test(
    'an authenticated owner of the part can not update the active settings record to a settings record that is not connected to the part',
    function () {
        $this->actingAs($this->user);

        $this->assertNull($this->rockShoxLyric->activeSettingsRecord);
        updateActiveSettingsRecord(
            $this->fox36->settingsRecords->get(0)->id
        )->assertAuthorizationError();
    }
);

test('an unauthorized user can not update the part', function () {
    $this->actingAs($this->user2);

    updatePart()->assertAuthorizationError();
});

test('an unauthenticated user can not update a part', function () {
    updatePart()->assertUnauthenticated();
});
