<?php

use App\Models\Part;
use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->user2 = User::factory()->create();
    $this->rockShoxLyric = Part::factory()
        ->rockShoxLyric()
        ->create([
            'user_id' => $this->user->id,
        ]);
    $this->rockShoxZeb = Part::factory()
        ->rockShoxZeb()
        ->create([
            'user_id' => $this->user2->id,
        ]);
    $this->rockShoxSuperDeluxe = Part::factory()
        ->rockShoxSuperDeluxe()
        ->create([
            'user_id' => $this->user->id,
        ]);
    $this->fox36 = Part::factory()
        ->fox36()
        ->create([
            'user_id' => $this->user->id,
        ]);
    $this->foxFloatX2 = Part::factory()
        ->foxFloatX2()
        ->create([
            'user_id' => $this->user->id,
        ]);
});

function queryPartById()
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        query PartById($partId: ID!) {
            partById(id: $partId) {
                id
                createdAt
                updatedAt
                name
                type
                manufacturer {
                    id
                }
                activeSettingsRecord {
                    id
                }
                user {
                    id
                }
                settingsRecords {
                    id
                }
                specificationsRecord {
                    id
                }
            }
        }
        ',
        ['partId' => test()->rockShoxLyric->id]
    );
}

function queryForks($first = 25, $page = 1)
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        query PartsByType($first: Int!, $page: Int!) {
            partsByType(type: FORK, first: $first, page: $page) {
                data {
                    id
                    createdAt
                    updatedAt
                    name
                    type
                    manufacturer {
                        id
                    }
                    activeSettingsRecord {
                        id
                    }
                    user {
                        id
                    }
                    settingsRecords {
                        id
                    }
                    specificationsRecord {
                        id
                    }
                }
                paginatorInfo {
                    lastItem
                    total
                }
            }
        }
        ',
        [
            'first' => $first,
            'page' => $page,
        ]
    );
}

function queryShocks($first = 25, $page = 1)
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        query PartsByType($first: Int!, $page: Int!) {
            partsByType(type: SHOCK, first: $first, page: $page) {
                data {
                    id
                    createdAt
                    updatedAt
                    name
                    type
                    manufacturer {
                        id
                    }
                    activeSettingsRecord {
                        id
                    }
                    user {
                        id
                    }
                    settingsRecords {
                        id
                    }
                    specificationsRecord {
                        id
                    }
                }
                paginatorInfo {
                    lastItem
                    total
                }
            }
        }
        ',
        [
            'first' => $first,
            'page' => $page,
        ]
    );
}

test('an authenticated user can query a part by id', function () {
    $this->actingAs($this->user);

    queryPartById($this->rockShoxLyric->id)
        ->assertJson([
            'data' => [
                'partById' => [
                    'id' => $this->rockShoxLyric->id,
                    'name' => 'Lyric',
                    'type' => 'FORK',
                    'manufacturer' => [
                        'id' => $this->rockShoxLyric->manufacturer->id,
                    ],
                    'activeSettingsRecord' => null,
                    'user' => [
                        'id' => $this->user->id,
                    ],
                    'settingsRecords' => [
                        [
                            'id' => $this->rockShoxLyric->settingsRecords->get(
                                0
                            )->id,
                        ],
                        [
                            'id' => $this->rockShoxLyric->settingsRecords->get(
                                1
                            )->id,
                        ],
                    ],
                    'specificationsRecord' => [
                        'id' => $this->rockShoxLyric->specificationsRecord->id,
                    ],
                ],
            ],
        ])
        ->assertHasTimestamps('partById');
});

test('an authenticated user can query parts by type', function () {
    $this->actingAs($this->user);

    queryForks()
        ->assertJson([
            'data' => [
                'partsByType' => [
                    'data' => [
                        [
                            'id' => $this->rockShoxLyric->id,
                            'name' => 'Lyric',
                            'type' => 'FORK',
                            'manufacturer' => [
                                'id' => $this->rockShoxLyric->manufacturer->id,
                            ],
                            'activeSettingsRecord' => null,
                            'user' => [
                                'id' => $this->user->id,
                            ],
                            'settingsRecords' => [
                                [
                                    'id' => $this->rockShoxLyric->settingsRecords->get(
                                        0
                                    )->id,
                                ],
                                [
                                    'id' => $this->rockShoxLyric->settingsRecords->get(
                                        1
                                    )->id,
                                ],
                            ],
                            'specificationsRecord' => [
                                'id' =>
                                    $this->rockShoxLyric->specificationsRecord
                                        ->id,
                            ],
                        ],
                        [
                            'id' => $this->fox36->id,
                            'name' => '36',
                            'type' => 'FORK',
                            'manufacturer' => [
                                'id' => $this->fox36->manufacturer->id,
                            ],
                            'activeSettingsRecord' => null,
                            'user' => [
                                'id' => $this->user->id,
                            ],
                            'settingsRecords' => [
                                [
                                    'id' => $this->fox36->settingsRecords->get(
                                        0
                                    )->id,
                                ],
                                [
                                    'id' => $this->fox36->settingsRecords->get(
                                        1
                                    )->id,
                                ],
                            ],
                            'specificationsRecord' => [
                                'id' => $this->fox36->specificationsRecord->id,
                            ],
                        ],
                    ],
                    'paginatorInfo' => [
                        'lastItem' => 2,
                        'total' => 2,
                    ],
                ],
            ],
        ])
        ->assertJsonCount(2, 'data.partsByType');

    queryShocks()
        ->assertJson([
            'data' => [
                'partsByType' => [
                    'data' => [
                        [
                            'id' => $this->rockShoxSuperDeluxe->id,
                            'name' => 'Super Deluxe',
                            'type' => 'SHOCK',
                            'manufacturer' => [
                                'id' =>
                                    $this->rockShoxSuperDeluxe->manufacturer
                                        ->id,
                            ],
                            'activeSettingsRecord' => null,
                            'user' => [
                                'id' => $this->user->id,
                            ],
                            'settingsRecords' => [
                                [
                                    'id' => $this->rockShoxSuperDeluxe->settingsRecords->get(
                                        0
                                    )->id,
                                ],
                                [
                                    'id' => $this->rockShoxSuperDeluxe->settingsRecords->get(
                                        1
                                    )->id,
                                ],
                            ],
                            'specificationsRecord' => [
                                'id' =>
                                    $this->rockShoxSuperDeluxe
                                        ->specificationsRecord->id,
                            ],
                        ],
                        [
                            'id' => $this->foxFloatX2->id,
                            'name' => 'Float X2',
                            'type' => 'SHOCK',
                            'manufacturer' => [
                                'id' => $this->foxFloatX2->manufacturer->id,
                            ],
                            'activeSettingsRecord' => null,
                            'user' => [
                                'id' => $this->user->id,
                            ],
                            'settingsRecords' => [
                                [
                                    'id' => $this->foxFloatX2->settingsRecords->get(
                                        0
                                    )->id,
                                ],
                                [
                                    'id' => $this->foxFloatX2->settingsRecords->get(
                                        1
                                    )->id,
                                ],
                            ],
                            'specificationsRecord' => [
                                'id' =>
                                    $this->foxFloatX2->specificationsRecord->id,
                            ],
                        ],
                    ],
                    'paginatorInfo' => [
                        'lastItem' => 2,
                        'total' => 2,
                    ],
                ],
            ],
        ])
        ->assertJsonCount(2, 'data.partsByType');
});

test('an unauthorized user can not query a part by id', function () {
    $this->actingAs($this->user2);

    queryPartById($this->rockShoxLyric->id)->assertAuthorizationError();
});

test('an unauthenticated user can not query a part by id', function () {
    queryPartById($this->rockShoxLyric->id)->assertUnauthenticated();
});

test('an unauthenticated user can not query a parts by type', function () {
    queryForks()->assertUnauthenticated();
});
