<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->user2 = User::factory()->create();
    $this->personalAccessToken = $this->user->createToken(
        'Foo bar'
    )->accessToken;
    $this->personalAccessToken2 = $this->user2->createToken(
        'Foo bar'
    )->accessToken;
});

function deletePersonalAccessToken($id, $headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation DeletePersonalAccessToken($id: ID!) {
            deletePersonalAccessToken(input: { id: $id }) {
                id
                name
                lastUsedAt
                createdAt
                updatedAt
            }
        }              
        ',
        [
            'id' => $id,
        ],
        [],
        $headers
    );
}

test('an authenticated user can delete a personal access token', function () {
    $this->actingAs($this->user);

    deletePersonalAccessToken(
        id: $this->personalAccessToken['id']
    )->assertJsonStructure([
        'data' => [
            'deletePersonalAccessToken' => [
                'id',
                'name',
                'lastUsedAt',
                'createdAt',
                'updatedAt',
            ],
        ],
    ]);

    expect(
        $this->user
            ->tokens()
            ->get()
            ->toArray()
    )->toBeEmpty();
});

test(
    'a user authenticated with a personal access token can not delete a personal access token',
    function () {
        deletePersonalAccessToken(
            id: $this->personalAccessToken['id'],
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test(
    'an unauthenticated user can not delete a personal access token',
    function () {
        deletePersonalAccessToken(
            id: $this->personalAccessToken['id']
        )->assertUnauthenticated();
    }
);

test(
    'an authenticated user can not delete a personal access token they do not own',
    function () {
        $this->actingAs($this->user);

        deletePersonalAccessToken(
            id: $this->personalAccessToken2['id']
        )->assertJson([
            'errors' => [
                [
                    'message' => 'No query results for model.',
                ],
            ],
        ]);
    }
);
