<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->personalAccessToken = $this->user->createToken(
        'Foo bar'
    )->accessToken;
    $this->personalAccessToken2 = $this->user->createToken(
        'Foo bar baz'
    )->accessToken;
});

function personalAccessTokens($page = 1)
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        query PersonalAccessTokens($page: Int) {
            personalAccessTokens(first: 1, page: $page) {
                data {
                    id
                    name
                    lastUsedAt
                    createdAt
                    updatedAt
                }
                paginatorInfo {
                    lastItem
                    total
                }
            }
        }        
        ',
        ['page' => $page]
    );
}

test(
    'an authenticated user can query their personal access tokens',
    function () {
        $this->actingAs($this->user);

        personalAccessTokens()->assertJson([
            'data' => [
                'personalAccessTokens' => [
                    'data' => [
                        [
                            'id' => $this->personalAccessToken->id,
                            'name' => $this->personalAccessToken->name,
                            'lastUsedAt' =>
                                $this->personalAccessToken->last_used_at,
                            'createdAt' =>
                                $this->personalAccessToken->created_at,
                            'updatedAt' =>
                                $this->personalAccessToken->updated_at,
                        ],
                    ],
                    'paginatorInfo' => [
                        'lastItem' => 1,
                        'total' => 2,
                    ],
                ],
            ],
        ]);
    }
);

test('pagination works', function () {
    $this->actingAs($this->user);

    personalAccessTokens(page: 2)->assertJson([
        'data' => [
            'personalAccessTokens' => [
                'data' => [
                    [
                        'id' => $this->personalAccessToken2->id,
                        'name' => $this->personalAccessToken2->name,
                        'lastUsedAt' =>
                            $this->personalAccessToken2->last_used_at,
                        'createdAt' => $this->personalAccessToken2->created_at,
                        'updatedAt' => $this->personalAccessToken2->updated_at,
                    ],
                ],
                'paginatorInfo' => [
                    'lastItem' => 2,
                    'total' => 2,
                ],
            ],
        ],
    ]);
});

test(
    'an unauthenticated user can not query personal access tokens',
    function () {
        personalAccessTokens()->assertUnauthenticated();
    }
);
