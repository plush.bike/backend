<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
});

function createPersonalAccessToken($name = 'Foo bar', $headers = [])
{
    return test()->graphQL(
        /** @lang GraphQL */ '
        mutation CreatePersonalAccessToken($name: String) {
            createPersonalAccessToken(input: { name: $name }) {
                id
                name
                token
                lastUsedAt
                createdAt
                updatedAt
            }
        }        
        ',
        [
            'name' => $name,
        ],
        [],
        $headers
    );
}

test('an authenticated user can create a personal access token', function () {
    $this->actingAs($this->user);

    createPersonalAccessToken()->assertJsonStructure([
        'data' => [
            'createPersonalAccessToken' => [
                'id',
                'name',
                'token',
                'lastUsedAt',
                'createdAt',
                'updatedAt',
            ],
        ],
    ]);
});

test(
    'a user authenticated with a personal access token can not create a personal access token',
    function () {
        createPersonalAccessToken(
            headers: test()->personalAccessTokenHeader()
        )->assertDisallowPersonalAccessTokenAuthentication();
    }
);

test(
    'an unauthenticated user can not create a personal access token',
    function () {
        createPersonalAccessToken()->assertUnauthenticated();
    }
);

test('name field is required', function () {
    $this->actingAs($this->user);

    createPersonalAccessToken(name: '')->assertValidationErrors([
        'name' => ['Name is required.'],
    ]);
});
