<?php

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

beforeEach(function () {
    Storage::fake();

    $this->user = User::factory()->create();
});

$defaultImages = [
    UploadedFile::fake()->image('part-image1.jpg'),
    UploadedFile::fake()->image('part-image2.jpg'),
];

function uploadPartImages($files)
{
    $operations = [
        'query' => /** @lang GraphQL */ '
            mutation UploadPartImages($files: [Upload!]!) {
                uploadPartImages(input: { files: $files }) {
                    src
                }
            }
        ',
        'variables' => [
            'files' => null,
        ],
    ];

    return test()->multipartGraphQL(
        $operations,
        [
            '0' => ['variables.files'],
        ],
        [
            '0' => $files,
        ]
    );
}

test('an authenticated user can upload a part image', function () use (
    $defaultImages
) {
    $this->actingAs($this->user);

    $expectedFiles = Storage::allFiles("parts/{$this->user->id}");

    uploadPartImages(files: $defaultImages)->assertJson([
        'data' => [
            'uploadPartImages' => array_map(function ($imageSrc) {
                return [
                    'src' => $imageSrc,
                ];
            }, $expectedFiles),
        ],
    ]);
});

test('files must be less than 10MB', function () {
    $this->actingAs($this->user);

    uploadPartImages(
        files: [
            UploadedFile::fake()
                ->image('profile-image.jpg')
                ->size(11000),
        ]
    )->assertValidationErrors([
        'files.0' => ['File must be less than 10MB.'],
    ]);
});

test('files must be an image', function () {
    $this->actingAs($this->user);

    uploadPartImages(
        files: [UploadedFile::fake()->create('profile-image.pdf')]
    )->assertValidationErrors([
        'files.0' => [
            'File must be an image (jpg, jpeg, png, bmp, gif, svg, or webp).',
        ],
    ]);
});

test('an unauthenticated user can not upload a part image', function () use (
    $defaultImages
) {
    uploadPartImages(files: $defaultImages)->assertUnauthenticated();
});
