<?php

namespace Tests;

use Illuminate\Testing\TestResponse as BaseTestResponse;

class TestResponse extends BaseTestResponse
{
    /**
     * Assert authorization error
     */
    public function assertAuthorizationError()
    {
        return $this->assertJsonFragment([
            'message' => 'This action is unauthorized.',
        ]);
    }

    /**
     * Assert authentication error
     */
    public function assertUnauthenticated()
    {
        return $this->assertUnauthorized();
    }

    /**
     * Assert authentication error
     */
    public function assertDisallowPersonalAccessTokenAuthentication()
    {
        return $this->assertUnauthorized()->assertJsonFragment([
            'message' =>
                'Unauthenticated. This action can not be authenticated with a personal access token.',
        ]);
    }

    /**
     * Assert that response has `createdAt` and `updatedAt` properties
     */
    public function assertHasTimestamps(string $queryName)
    {
        return $this->assertJsonStructure([
            'data' => [
                $queryName => ['createdAt', 'updatedAt'],
            ],
        ]);
    }

    public function assertValidationErrors(array $errors)
    {
        return $this->assertJson([
            'errors' => [
                [
                    'extensions' => [
                        'validation' => array_combine(
                            array_map(function ($fieldName) {
                                return "input.{$fieldName}";
                            }, array_keys($errors)),
                            $errors
                        ),
                    ],
                ],
            ],
        ]);
    }
}
