<?php

use App\Models\Part;
use App\Models\PartSettingsRecord;

beforeEach(function () {
    $this->part = Part::factory()->create();
    $this->settingsRecord = PartSettingsRecord::factory()->create([
        'part_id' => $this->part,
    ]);
    $this->part->active_settings_record_id = $this->settingsRecord->id;
    $this->part->save();
});

test('part', function () {
    expect(
        $this->settingsRecord
            ->part()
            ->first()
            ->toArray()
    )->toMatchArray($this->part->toArray());
});

test('activeSettingPart', function () {
    expect(
        $this->settingsRecord
            ->activeSettingPart()
            ->first()
            ->toArray()
    )->toMatchArray($this->part->toArray());
});
