<?php

use App\Models\Part;
use App\Models\PartSpecificationsRecord;

beforeEach(function () {
    $this->part = Part::factory()->create();
    $this->specificationsRecord = PartSpecificationsRecord::factory()->create([
        'part_id' => $this->part,
    ]);
});

test('part', function () {
    expect(
        $this->specificationsRecord
            ->part()
            ->first()
            ->toArray()
    )->toMatchArray($this->part->toArray());
});
