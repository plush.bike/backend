<?php

use App\Models\Part;
use App\Models\PartImage;

beforeEach(function () {
    $this->part = Part::factory()
        ->fox36()
        ->create();
    $this->partImage = PartImage::factory()->create([
        'part_id' => $this->part,
    ]);
});

test('part', function () {
    expect(
        $this->partImage
            ->part()
            ->first()
            ->toArray()
    )->toMatchArray($this->part->toArray());
});

test('url', function () {
    expect($this->partImage->url)->toBe(
        "https://ik.imagekit.io/{$this->partImage->src}"
    );
});
