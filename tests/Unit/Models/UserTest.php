<?php

use App\Models\Part;
use App\Models\SocialProvider;
use App\Models\User;
use App\Notifications\ResetPassword;
use Illuminate\Support\Facades\Notification;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->part = Part::factory()->create([
        'user_id' => $this->user,
    ]);
    $this->socialProvider = SocialProvider::factory()->create([
        'user_id' => $this->user,
    ]);
});

test('parts', function () {
    expect($this->user->parts->toArray())->toMatchArray([
        $this->part->toArray(),
    ]);
});

test('sendPasswordResetNotification', function () {
    Notification::fake();

    $this->user->sendPasswordResetNotification('token');

    Notification::assertSentTo($this->user, ResetPassword::class);
});

test('`profileImageUrl` when `profile_image_src` is `null`', function () {
    expect($this->user->profile_image_url)->toBeNull();
});

test('`profileImageUrl` when `profile_image_src` is set', function () {
    $profileImageSrc =
        'profile_images/1/V2UAGLjCDN7Dp0x8ZeaYrPnBBy5Kl4MErMsOIPGl.png';
    $this->user->update([
        'profile_image_src' => $profileImageSrc,
    ]);

    expect($this->user->profile_image_url)->toBe(
        "https://ik.imagekit.io/{$profileImageSrc}"
    );
});

test('socialProviders', function () {
    expect($this->user->socialProviders->toArray())->toMatchArray([
        $this->socialProvider->toArray(),
    ]);
});

test('has_password_set', function () {
    expect($this->user->has_password_set)->toBe(true);
    expect(
        User::factory()->create(['password' => null])->has_password_set
    )->toBe(false);
});
