<?php

use App\Models\Manufacturer;
use App\Models\Part;

beforeEach(function () {
    $this->fox = Manufacturer::factory()
        ->fox()
        ->create();
    $this->fox36 = Part::factory()->create([
        'name' => '36',
        'manufacturer_id' => $this->fox,
    ]);
});

test('parts', function () {
    expect(
        $this->fox
            ->parts()
            ->get()
            ->toArray()
    )->toMatchArray([$this->fox36->toArray()]);
});
