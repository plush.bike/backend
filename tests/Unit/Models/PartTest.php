<?php

use App\Models\Part;
use App\Models\User;
use App\Enums\PartType;
use App\Models\Manufacturer;
use App\Models\PartImage;
use App\Models\PartSettingsRecord;
use App\Models\PartSpecificationsRecord;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->manufacturer = Manufacturer::factory()
        ->fox()
        ->create();
    $this->part = Part::factory()->create([
        'user_id' => $this->user,
        'manufacturer_id' => $this->manufacturer,
        'name' => '36',
        'type' => PartType::FORK,
    ]);
    $this->specificationsRecord = PartSpecificationsRecord::factory()->create([
        'part_id' => $this->part,
    ]);
    $this->settingsRecords = PartSettingsRecord::factory()
        ->count(2)
        ->create([
            'part_id' => $this->part,
        ]);
    $this->partImages = PartImage::factory()
        ->count(2)
        ->create(['part_id' => $this->part]);
    $this->activeSettingsRecord = $this->settingsRecords->get(0);
    $this->part->active_settings_record_id = $this->activeSettingsRecord->id;
    $this->part->save();
});

test('user', function () {
    expect(
        $this->part
            ->user()
            ->first()
            ->toArray()
    )->toMatchArray($this->user->toArray());
});

test('manufacturer', function () {
    expect(
        $this->part
            ->manufacturer()
            ->first()
            ->toArray()
    )->toMatchArray($this->manufacturer->toArray());
});

test('settingsRecords', function () {
    expect(
        $this->part
            ->settingsRecords()
            ->get()
            ->toArray()
    )->toMatchArray($this->settingsRecords->toArray());
});

test('activeSettingsRecord', function () {
    expect(
        $this->part
            ->activeSettingsRecord()
            ->first()
            ->toArray()
    )->toMatchArray($this->activeSettingsRecord->toArray());
});

test('specificationsRecord', function () {
    expect($this->part->specificationsRecord->toArray())->toMatchArray(
        $this->specificationsRecord->toArray()
    );
});

test('images', function () {
    expect(
        $this->part
            ->images()
            ->get()
            ->toArray()
    )->toMatchArray($this->partImages->toArray());
});
