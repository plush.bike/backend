<?php

use App\Models\SocialProvider;
use App\Models\User;
use Illuminate\Support\Carbon;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->socialProvider = SocialProvider::factory()->create([
        'user_id' => $this->user,
        'token' => 'fake-token',
        'refresh_token' => 'fake-refresh-token',
    ]);
});

test('user', function () {
    expect($this->socialProvider->user->id)->toBe($this->user->id);
});

test('token', function () {
    $this->assertEncryptedUnserialized(
        'social_providers',
        ['id' => $this->socialProvider->id],
        [
            'token' => 'fake-token',
        ]
    );
});

test('refresh_token', function () {
    $this->assertEncryptedUnserialized(
        'social_providers',
        ['id' => $this->socialProvider->id],
        [
            'refresh_token' => 'fake-refresh-token',
        ]
    );
});

test('expires_at', function () {
    $this->freezeTime(function (Carbon $time) {
        $this->socialProvider->update([
            'expires_at' => 3600,
        ]);

        expect($this->socialProvider->expires_at->getTimestamp())->toBe(
            $time->addHour()->getTimestamp()
        );
    });
});
