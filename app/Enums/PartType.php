<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PartType extends Enum
{
    const FORK = 'FORK';
    const SHOCK = 'SHOCK';
}
