<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SocialProviderType extends Enum
{
    const STRAVA = 'STRAVA';
    const GOOGLE = 'GOOGLE';
}
