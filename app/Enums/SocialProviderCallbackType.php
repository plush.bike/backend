<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SocialProviderCallbackType extends Enum
{
    const SIGN_IN = 'SIGN_IN';
    const CONNECT = 'CONNECT';
}
