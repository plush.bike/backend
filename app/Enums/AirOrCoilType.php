<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AirOrCoilType extends Enum
{
    const AIR = 'AIR';
    const COIL = 'COIL';
}
