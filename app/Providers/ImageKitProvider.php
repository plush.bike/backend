<?php

namespace App\Providers;

use ImageKit\ImageKit;
use Illuminate\Support\ServiceProvider;

class ImageKitProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ImageKit::class, function () {
            return new ImageKit(
                config('imagekit.public_key'),
                config('imagekit.private_key'),
                config('imagekit.endpoint')
            );
        });
    }
}
