<?php

namespace App\Providers;

use App\Exceptions\SocialProviderException;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Models\SocialProvider;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Fortify::authenticateUsing(function (Request $request) {
            if ($request->email && $request->password) {
                return $this->authenticateWithEmailAndPassword($request);
            }

            if ($request->code && $request->provider) {
                return $this->authenticateWithSocialProvider($request);
            }
        });
    }

    protected function authenticateWithEmailAndPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (
            !$user ||
            !Auth::guard('web')
                ->getProvider()
                ->validateCredentials($user, [
                    'password' => $request->password,
                ])
        ) {
            return null;
        }

        return $user;
    }

    protected function authenticateWithSocialProvider(Request $request)
    {
        try {
            /** @var Laravel\Socialite\Two\AbstractProvider */
            $socialiteProvider = Socialite::driver(
                strtolower($request->provider)
            );
            $socialProviderUser = $socialiteProvider->stateless()->user();
        } catch (\Exception $e) {
            $formattedProviderTitle = ucwords(strtolower($request->provider));

            throw new SocialProviderException(
                "Could not communicate with {$formattedProviderTitle}."
            );
        }

        $socialProvider = SocialProvider::where('provider', $request->provider)
            ->where('provider_user_id', $socialProviderUser->getId())
            ->first();

        if ($socialProvider !== null) {
            return $socialProvider->user()->first();
        }

        if (
            $socialProviderUser->getEmail() !== null &&
            User::where('email', $socialProviderUser->getEmail())->exists()
        ) {
            throw new SocialProviderException(
                'An account with this email already exists.'
            );
        }

        $user = User::create([
            'name' => $socialProviderUser->getName(),
            'email' => $socialProviderUser->getEmail(),
            'provisioned_by_social_provider' => true,
        ]);

        SocialProvider::create([
            'user_id' => $user->id,
            'provider' => $request->provider,
            'provider_user_id' => $socialProviderUser->getId(),
            'token' => $socialProviderUser->token,
            'refresh_token' => $socialProviderUser->refreshToken,
            'expires_at' => $socialProviderUser->expiresIn,
        ]);

        return $user;
    }
}
