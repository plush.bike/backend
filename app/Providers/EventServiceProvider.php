<?php

namespace App\Providers;

use App\Models\Part;
use App\Models\PartSettingsRecord;
use App\Observers\PartObserver;
use App\Observers\PartSettingsRecordObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [SendEmailVerificationNotification::class],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            \SocialiteProviders\Strava\StravaExtendSocialite::class . '@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Part::observe(PartObserver::class);
        PartSettingsRecord::observe(PartSettingsRecordObserver::class);
    }
}
