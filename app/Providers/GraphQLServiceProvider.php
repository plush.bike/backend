<?php

namespace App\Providers;

use App\Enums\AirOrCoilType;
use App\Enums\PartType;
use App\Enums\SocialProviderCallbackType;
use App\Enums\SocialProviderType;
use Illuminate\Support\ServiceProvider;
use Nuwave\Lighthouse\Schema\TypeRegistry;
use Nuwave\Lighthouse\Schema\Types\LaravelEnumType;

class GraphQLServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param TypeRegistry $typeRegistry
     *
     * @return void
     */
    public function boot(TypeRegistry $typeRegistry): void
    {
        $typeRegistry->register(new LaravelEnumType(PartType::class));
        $typeRegistry->register(new LaravelEnumType(SocialProviderType::class));
        $typeRegistry->register(
            new LaravelEnumType(SocialProviderCallbackType::class)
        );
        $typeRegistry->register(new LaravelEnumType(AirOrCoilType::class));
    }
}
