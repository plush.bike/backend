<?php

namespace App\Exceptions;

use Exception;

class SocialProviderException extends Exception
{
    public $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }
}
