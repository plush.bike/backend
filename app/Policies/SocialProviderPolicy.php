<?php

namespace App\Policies;

use App\Models\SocialProvider;
use App\Models\User;
use App\Policies\BasePolicy;

class SocialProviderPolicy extends BasePolicy
{
    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Part  $part
     * @return mixed
     */
    public function delete(User $user, SocialProvider $socialProvider)
    {
        return $this->isOwner($user, $socialProvider) &&
            ($user->has_password_set ||
                $user
                    ->socialProviders()
                    ->where('provider', '!=', $socialProvider->provider)
                    ->exists());
    }
}
