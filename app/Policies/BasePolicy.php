<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    protected function isOwner(User $user, $model)
    {
        if ($model->user_id === null) {
            return false;
        }

        return $model->user_id === $user->id;
    }
}
