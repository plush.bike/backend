<?php

namespace App\Policies;

use App\Models\Part;
use App\Models\User;
use App\Policies\BasePolicy;

class PartPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Part  $part
     * @return mixed
     */
    public function view(User $user, Part $part)
    {
        return $this->isOwner($user, $part);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Part  $part
     * @return mixed
     */
    public function update(User $user, Part $part, array $injected)
    {
        if (!$this->isOwner($user, $part)) {
            return false;
        }

        if (isset($injected['activeSettingsRecord']['connect'])) {
            return $part
                ->settingsRecords()
                ->where('id', $injected['activeSettingsRecord']['connect'])
                ->exists();
        }

        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Part  $part
     * @return mixed
     */
    public function delete(User $user, Part $part)
    {
        return $this->isOwner($user, $part);
    }
}
