<?php

namespace App\Policies;

use App\Models\PartSpecificationsRecord;
use App\Models\User;
use App\Policies\BasePolicy;

class PartSpecificationsRecordPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PartSpecificationsRecord  $specificationsRecord
     * @return mixed
     */
    public function view(
        User $user,
        PartSpecificationsRecord $specificationsRecord
    ) {
        return $this->isOwner($user, $specificationsRecord->part()->first());
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\PartSpecificationsRecord  $specificationsRecord
     * @return mixed
     */
    public function update(
        User $user,
        PartSpecificationsRecord $specificationsRecord
    ) {
        return $this->isOwner($user, $specificationsRecord->part()->first());
    }
}
