<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPassword extends ResetPasswordNotification
{
    public static $createUrlCallback = [self::class, 'generateUrlCallback'];

    public static function generateUrlCallback($notifiable, $token)
    {
        return config('frontend.app_url') .
            '/forgot-password/reset?' .
            http_build_query([
                'token' => $token,
                'email' => $notifiable->getEmailForPasswordReset(),
            ]);
    }
}
