<?php

namespace App\Models;

use App\Enums\PartType;
use App\Models\User;
use App\Models\Manufacturer;
use App\Models\PartSettingsRecord;
use App\Models\PartImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Part extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $casts = [
        'type' => PartType::class,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function manufacturer(): BelongsTo
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function settingsRecords(): HasMany
    {
        return $this->hasMany(PartSettingsRecord::class);
    }

    public function activeSettingsRecord(): BelongsTo
    {
        return $this->belongsTo(PartSettingsRecord::class);
    }

    public function specificationsRecord(): HasOne
    {
        return $this->hasOne(PartSpecificationsRecord::class);
    }

    public function images(): HasMany
    {
        return $this->hasMany(PartImage::class);
    }
}
