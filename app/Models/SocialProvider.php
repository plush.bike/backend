<?php

namespace App\Models;

use App\Enums\SocialProviderType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Carbon;

class SocialProvider extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
        'token',
        'refresh_token',
        'expires_at',
        'provisioned_by_social_provider',
    ];

    protected $casts = [
        'provider' => SocialProviderType::class,
        'token' => 'encrypted',
        'refresh_token' => 'encrypted',
        'expires_at' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Interact with expires_at.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function expiresAt(): Attribute
    {
        return Attribute::make(
            set: fn($value) => empty($value)
                ? null
                : Carbon::now()->addSeconds($value)
        );
    }
}
