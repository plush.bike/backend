<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use ImageKit\ImageKit;

class PartImage extends Model
{
    use HasFactory;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function part(): BelongsTo
    {
        return $this->belongsTo(Part::class);
    }

    public function getUrlAttribute()
    {
        return app()
            ->make(ImageKit::class)
            ->url([
                'path' => $this->src,
                'signed' => true,
                'expireSeconds' => 300,
            ]);
    }
}
