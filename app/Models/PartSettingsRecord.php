<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Part;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PartSettingsRecord extends Model
{
    use HasFactory;

    public function part(): BelongsTo
    {
        return $this->belongsTo(Part::class);
    }

    public function activeSettingPart(): HasOne
    {
        return $this->hasOne(Part::class, 'active_settings_record_id');
    }
}
