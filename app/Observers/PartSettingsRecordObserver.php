<?php

namespace App\Observers;

use App\Models\PartSettingsRecord;

class PartSettingsRecordObserver
{
    /**
     * Handle the PartSettingsRecord "deleting" event.
     *
     * @param  \App\Models\PartSettingsRecord  $partSettingsRecord
     * @return void
     */
    public function deleting(PartSettingsRecord $partSettingsRecord)
    {
        $activeSettingPart = $partSettingsRecord->activeSettingPart();

        if (is_null($activeSettingPart)) {
            return;
        }

        $activeSettingPart->update([
            'active_settings_record_id' => null,
        ]);
    }
}
