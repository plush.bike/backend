<?php

namespace App\Observers;

use App\Models\Part;
use Illuminate\Support\Facades\Storage;

class PartObserver
{
    private function imageSrc($image)
    {
        return $image['src'];
    }

    /**
     * Handle the Part "deleting" event.
     *
     * @param  \App\Models\Part  $part
     * @return void
     */
    public function deleting(Part $part)
    {
        Storage::delete(
            array_map([$this, 'imageSrc'], $part->images->toArray())
        );
    }
}
