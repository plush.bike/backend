<?php declare(strict_types=1);

namespace App\GraphQL\Exceptions;

use Nuwave\Lighthouse\Exceptions\AuthenticationException as LighthouseAuthenticationException;

class AuthenticationException extends LighthouseAuthenticationException
{
}
