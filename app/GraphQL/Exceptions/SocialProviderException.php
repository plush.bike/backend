<?php

namespace App\GraphQL\Exceptions;

use Exception;
use GraphQL\Error\ClientAware;
use GraphQL\Error\ProvidesExtensions;
use App\Enums\SocialProviderCallbackType;

class SocialProviderException extends Exception implements
    ClientAware,
    ProvidesExtensions
{
    /**
     * @var SocialProviderCallbackType
     */
    private $callbackType;

    public function __construct(string $message, string $callbackType)
    {
        parent::__construct($message);

        $this->callbackType = $callbackType;
    }

    /**
     * Returns true when exception message is safe to be displayed to a client.
     *
     * @api
     *
     * @return bool
     */
    public function isClientSafe(): bool
    {
        return true;
    }

    /**
     * Return the content that is put in the "extensions" part
     * of the returned error.
     *
     * @return array
     */
    public function getExtensions(): array
    {
        return [
            'callbackType' => $this->callbackType,
        ];
    }
}
