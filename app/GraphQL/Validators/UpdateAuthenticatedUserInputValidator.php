<?php

namespace App\GraphQL\Validators;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Nuwave\Lighthouse\Validation\Validator;

class UpdateAuthenticatedUserInputValidator extends Validator
{
    /**
     * Return the validation rules.
     *
     * @return array<string, array<mixed>>
     */
    public function rules(): array
    {
        $authenticatedUser = Auth::user();

        if (!$authenticatedUser) {
            return [];
        }

        return [
            'email' => [
                'email',
                Rule::unique('users', 'email')->ignore(
                    $authenticatedUser->id,
                    'id'
                ),
            ],
            'profileImage' => ['nullable', 'image', 'max:10000'],
        ];
    }

    /**
     * Return the custom messages
     *
     * @return array<string, array<mixed>>
     */
    public function messages(): array
    {
        return [];
    }
}
