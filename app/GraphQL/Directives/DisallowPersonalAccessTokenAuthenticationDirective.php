<?php

namespace App\GraphQL\Directives;

use Illuminate\Pipeline\Pipeline;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use App\Http\Middleware\DisallowPersonalAccessTokenAuthentication;

class DisallowPersonalAccessTokenAuthenticationDirective
    extends BaseDirective
    implements FieldMiddleware
{
    /**
     * @var \Illuminate\Pipeline\Pipeline
     */
    protected $pipeline;

    public function __construct(Pipeline $pipeline)
    {
        $this->pipeline = $pipeline;
    }

    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'GRAPHQL'
"""
Implements the Illuminate\Auth\Middleware\DisallowPersonalAccessTokenAuthentication middleware
"""
directive @disallowPersonalAccessTokenAuthentication on FIELD_DEFINITION
GRAPHQL;
    }

    /**
     * Wrap around the final field resolver.
     */
    public function handleField(FieldValue $fieldValue): void
    {
        $fieldValue->wrapResolver(
            fn(callable $previousResolver) => function (
                mixed $root,
                array $args,
                GraphQLContext $context,
                ResolveInfo $resolveInfo
            ) use ($previousResolver) {
                $this->pipeline
                    ->send($context->request())
                    ->through(DisallowPersonalAccessTokenAuthentication::class)
                    ->thenReturn();

                return $previousResolver($root, $args, $context, $resolveInfo);
            }
        );
    }
}
