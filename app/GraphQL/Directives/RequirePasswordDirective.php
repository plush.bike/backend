<?php

namespace App\GraphQL\Directives;

use App\GraphQL\Exceptions\PasswordRequiredException;
use Illuminate\Http\JsonResponse;
use Illuminate\Pipeline\Pipeline;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Illuminate\Auth\Middleware\RequirePassword as RequirePasswordMiddleware;

class RequirePasswordDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * @var \Illuminate\Pipeline\Pipeline
     */
    protected $pipeline;

    public function __construct(Pipeline $pipeline)
    {
        $this->pipeline = $pipeline;
    }

    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'GRAPHQL'
"""
Implements the Illuminate\Auth\Middleware\RequirePassword middleware
"""
directive @requirePassword on FIELD_DEFINITION
GRAPHQL;
    }

    /**
     * Wrap around the final field resolver.
     */
    public function handleField(FieldValue $fieldValue): void
    {
        $fieldValue->wrapResolver(
            fn(callable $previousResolver) => function (
                mixed $root,
                array $args,
                GraphQLContext $context,
                ResolveInfo $resolveInfo
            ) use ($previousResolver) {
                if (!$context->user()->has_password_set) {
                    return $previousResolver(
                        $root,
                        $args,
                        $context,
                        $resolveInfo
                    );
                }

                $result = $this->pipeline
                    ->send($context->request())
                    ->through(RequirePasswordMiddleware::class)
                    ->thenReturn();

                if ($result instanceof JsonResponse) {
                    throw new PasswordRequiredException(
                        'Please enter your password to continue.'
                    );
                }

                return $previousResolver($root, $args, $context, $resolveInfo);
            }
        );
    }
}
