<?php

namespace App\GraphQL\Mutations;

use GraphQL\Error\Error;
use Illuminate\Support\Facades\Auth;

class PersonalAccessToken
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function createPersonalAccessToken($_, array $args)
    {
        /** @var \App\Models\User */
        $authenticatedUser = Auth::user();

        $token = $authenticatedUser->createToken($args['name']);
        $accessToken = $token->accessToken;

        return [
            'id' => $accessToken['id'],
            'name' => $accessToken['name'],
            'token' => $token->plainTextToken,
            'lastUsedAt' => $accessToken['last_used_at'],
            'updatedAt' => $accessToken['updated_at'],
            'createdAt' => $accessToken['created_at'],
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function deletePersonalAccessToken($_, array $args)
    {
        /** @var \App\Models\User */
        $authenticatedUser = Auth::user();

        $token = $authenticatedUser
            ->tokens()
            ->where('id', $args['id'])
            ->first();

        if ($token === null) {
            throw new Error('No query results for model.');
        }

        $token->delete();

        return $token;
    }
}
