<?php

namespace App\GraphQL\Mutations;

use App\Enums\SocialProviderCallbackType;
use App\Exceptions\SocialProviderException;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Routing\Pipeline;
use Illuminate\Http\JsonResponse;
use App\GraphQL\Exceptions\SignInException;
use App\Http\Requests\SignInRequest;
use Illuminate\Support\Facades\Hash;
use App\GraphQL\Exceptions\ValidationException;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
use App\GraphQL\Exceptions\EmailNotSentException;
use App\GraphQL\Exceptions\SocialProviderException as SocialProviderGraphQLException;
use Illuminate\Auth\AuthenticationException;
use Laravel\Fortify\Actions\ConfirmPassword;
use Illuminate\Support\Facades\Auth as AuthFacade;
use Laravel\Fortify\Actions\AttemptToAuthenticate;
use App\GraphQL\Exceptions\TwoFactorAuthenticationException;
use App\Models\SocialProvider;
use Illuminate\Support\Facades\Log;
use Laravel\Fortify\Actions\PrepareAuthenticatedSession;
use Laravel\Fortify\Http\Requests\TwoFactorLoginRequest;
use Laravel\Fortify\Actions\EnableTwoFactorAuthentication;
use Laravel\Fortify\Actions\ConfirmTwoFactorAuthentication;
use Laravel\Fortify\Actions\DisableTwoFactorAuthentication;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;
use Laravel\Fortify\Actions\RedirectIfTwoFactorAuthenticatable;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Laravel\Socialite\Facades\Socialite;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Auth
{
    const MISSING_PRIMARY_KEY_FOR_UPDATE = 'Missing primary key for update.';

    protected function signInUser($user, $request)
    {
        $guard = AuthFacade::guard('web');
        $guard->login($user);

        $request->session()->regenerate();

        return [
            'user' => $guard->user(),
        ];
    }

    protected function signInPipeline($request)
    {
        // Authentication handled in `app/Providers/FortifyServiceProvider.php`
        $result = (new Pipeline(app()))
            ->send($request)
            ->through([
                RedirectIfTwoFactorAuthenticatable::class,
                AttemptToAuthenticate::class,
                PrepareAuthenticatedSession::class,
            ])
            ->thenReturn();

        if ($result instanceof JsonResponse) {
            if ($result->exception instanceof IlluminateValidationException) {
                throw new SignInException('Incorrect username or password.');
            }

            if ($result->exception instanceof SocialProviderException) {
                throw new SocialProviderGraphQLException(
                    $result->exception->getMessage(),
                    SocialProviderCallbackType::SIGN_IN
                );
            }

            if (array_key_exists('two_factor', $result->getData(true))) {
                return [
                    'user' => null,
                    'twoFactor' => true,
                ];
            }
        }

        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        return [
            'user' => $authenticatedUser,
            'twoFactor' => false,
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function signIn($_, array $args)
    {
        $signInRequest = app(SignInRequest::class);
        $signInRequest->replace([
            'email' => $args['email'],
            'password' => $args['password'],
            'remember' => false,
        ]);

        return $this->signInPipeline($signInRequest);
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function confirmPassword($_, array $args)
    {
        $guard = AuthFacade::guard('web');
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        $confirmed = app(ConfirmPassword::class)(
            $guard,
            $authenticatedUser,
            $args['password']
        );

        if ($confirmed) {
            Session::put('auth.password_confirmed_at', now()->getTimestamp());

            return [
                'status' => 'SUCCESS',
                'message' => 'Password confirmed successfully.',
            ];
        }

        throw new ValidationException(
            [
                'input.password' => ['Password is incorrect.'],
            ],
            'Password is incorrect.'
        );
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function signOut()
    {
        $guard = AuthFacade::guard('web');

        if (!$guard->check()) {
            throw new AuthenticationException();
        }

        $guard->logout();

        $session = Request::session();

        $session->invalidate();

        $session->regenerateToken();

        return [
            'status' => 'SUCCESS',
            'message' => 'Your session has been terminated.',
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function forgotPassword($_, array $args)
    {
        $status = Password::sendResetLink(['email' => $args['email']]);

        if ($status == Password::RESET_LINK_SENT) {
            return [
                'status' => 'SUCCESS',
                'message' => 'Reset password email sent successfully.',
            ];
        }

        throw new EmailNotSentException('Email not sent');
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function updateForgottenPassword($_, array $args)
    {
        $status = Password::reset(
            collect($args)
                ->only('email', 'password', 'password_confirmation', 'token')
                ->toArray(),
            function ($user) use ($args) {
                $user
                    ->forceFill([
                        'password' => Hash::make($args['password']),
                        'remember_token' => Str::random(60),
                    ])
                    ->save();
            }
        );

        if ($status === Password::PASSWORD_RESET) {
            return [
                'status' => 'SUCCESS',
                'message' => 'Password successfully updated.',
            ];
        }

        throw new ValidationException(
            [
                'input.token' => [$status],
            ],
            'An error has occurred while resetting the password.'
        );
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function signUp($_, array $args)
    {
        User::create([
            'name' => $args['name'],
            'email' => $args['email'],
            'password' => Hash::make($args['password']),
        ]);

        return [
            'status' => 'SUCCESS',
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function updatePassword($_, array $args)
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        if (
            $authenticatedUser->has_password_set &&
            !Hash::check($args['currentPassword'], $authenticatedUser->password)
        ) {
            throw new ValidationException(
                [
                    'input.currentPassword' => [
                        'Current password is incorrect.',
                    ],
                ],
                'Current password is incorrect.'
            );
        }

        $authenticatedUser->password = Hash::make($args['password']);
        $authenticatedUser->save();

        return [
            'status' => 'SUCCESS',
            'message' => 'Your password has been updated.',
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function updateAuthenticatedUser($_, array $args)
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        if (!array_key_exists('profileImage', $args)) {
            $authenticatedUser->update($args);

            return $authenticatedUser;
        }

        if (is_null($args['profileImage'])) {
            $authenticatedUser->update(
                array_merge(
                    ['profile_image_src' => null],
                    Arr::except($args, ['profileImage'])
                )
            );

            return $authenticatedUser;
        }

        $file = $args['profileImage'];

        $profileImageSrc = $file->store(
            "profile_images/{$authenticatedUser->id}",
            [
                'visibility' => 'private',
            ]
        );

        $authenticatedUser->update(
            array_merge(
                ['profile_image_src' => $profileImageSrc],
                Arr::except($args, ['profileImage'])
            )
        );

        return $authenticatedUser;
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function enableTwoFactorAuthentication()
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        $enable = new EnableTwoFactorAuthentication(
            app(TwoFactorAuthenticationProvider::class)
        );

        $enable($authenticatedUser);

        return [
            'status' => 'SUCCESS',
            'message' => 'Two factor authentication enabled.',
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function confirmTwoFactorAuthentication($_, array $args)
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        if (!is_null($authenticatedUser->two_factor_confirmed_at)) {
            throw new TwoFactorAuthenticationException(
                'Two factor authentication has already been confirmed.'
            );
        }

        $confirm = new ConfirmTwoFactorAuthentication(
            app(TwoFactorAuthenticationProvider::class)
        );

        try {
            $confirm($authenticatedUser, $args['code']);

            return [
                'status' => 'SUCCESS',
                'message' => 'Two factor authentication confirmed.',
                'recoveryCodes' => $authenticatedUser->recoveryCodes(),
            ];
        } catch (IlluminateValidationException $e) {
            throw new ValidationException(
                [
                    'input.code' => [
                        'The provided one time password was invalid.',
                    ],
                ],
                'An error has occurred while enabling two factor authentication.'
            );
        }
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function twoFactorAuthenticationQrCode()
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        if (is_null($authenticatedUser->two_factor_secret)) {
            throw new TwoFactorAuthenticationException(
                'You must enable two factor authentication before QR code can be generated.'
            );
        }

        if (!is_null($authenticatedUser->two_factor_confirmed_at)) {
            throw new TwoFactorAuthenticationException(
                'Two factor authentication has already been confirmed. QR code can not be generated.'
            );
        }

        return [
            'svg' => $authenticatedUser->twoFactorQrCodeSvg(),
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function disableTwoFactorAuthentication()
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        $disable = new DisableTwoFactorAuthentication();

        $disable($authenticatedUser);

        return [
            'status' => 'SUCCESS',
            'message' => 'Two factor authentication disabled.',
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function twoFactorAuthenticationRecoveryCodes()
    {
        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        if (!$authenticatedUser->hasEnabledTwoFactorAuthentication()) {
            throw new TwoFactorAuthenticationException(
                'You must enable two factor authentication before recovery codes can be viewed.'
            );
        }

        return [
            'recoveryCodes' => $authenticatedUser->recoveryCodes(),
        ];
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function twoFactorAuthenticationChallenge($_, array $args)
    {
        $request = app(TwoFactorLoginRequest::class);
        $request->replace([
            'recovery_code' => array_key_exists('recoveryCode', $args)
                ? $args['recoveryCode']
                : null,
            'code' => array_key_exists('code', $args) ? $args['code'] : null,
        ]);

        if (!$request->hasChallengedUser()) {
            throw new SignInException(
                'You must sign in before completing the two factor authentication challenge.'
            );
        }

        $user = $request->challengedUser();

        if ($request->recovery_code) {
            if ($request->validRecoveryCode()) {
                $user->replaceRecoveryCode($request->recovery_code);

                return $this->signInUser($user, $request);
            } else {
                throw new SignInException('Invalid recovery code.');
            }
        }

        if ($request->code) {
            if ($request->hasValidCode()) {
                return $this->signInUser($user, $request);
            } else {
                throw new SignInException('Invalid one time password.');
            }
        }

        throw new SignInException(
            'Please supply a one time password or recovery code.'
        );
    }

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function socialProviderCallback(
        $_,
        array $args,
        GraphQLContext $context
    ) {
        $request = $context->request();

        $request->replace([
            'code' => $args['code'],
            'provider' => $args['provider'],
        ]);

        /** @var \App\Models\User */
        $authenticatedUser = AuthFacade::user();

        $formattedProviderTitle = ucwords(strtolower($request->provider));

        if ($authenticatedUser !== null) {
            try {
                /** @var Laravel\Socialite\Two\AbstractProvider */
                $socialiteProvider = Socialite::driver(
                    strtolower($request->provider)
                );
                $socialProviderUser = $socialiteProvider->stateless()->user();
            } catch (\Exception $e) {
                throw new SocialProviderGraphQLException(
                    "Could not communicate with {$formattedProviderTitle}.",
                    SocialProviderCallbackType::CONNECT
                );
            }

            try {
                SocialProvider::create([
                    'user_id' => $authenticatedUser->id,
                    'provider' => $args['provider'],
                    'provider_user_id' => $socialProviderUser->getId(),
                    'token' => $socialProviderUser->token,
                    'refresh_token' => $socialProviderUser->refreshToken,
                    'expires_at' => $socialProviderUser->expiresIn,
                ]);
            } catch (\Exception $e) {
                throw new SocialProviderGraphQLException(
                    "Could not connect {$formattedProviderTitle} to your account.",
                    SocialProviderCallbackType::CONNECT
                );
            }

            return [
                'callbackType' => SocialProviderCallbackType::CONNECT,
            ];
        } else {
            $response = $this->signInPipeline($request);
            $response += [
                'callbackType' => SocialProviderCallbackType::SIGN_IN,
            ];

            return $response;
        }
    }
}
