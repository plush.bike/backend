<?php

namespace App\GraphQL\Mutations;

use Illuminate\Support\Facades\Auth;

class UploadPartImages
{
    /**
     * Return a value for the field.
     *
     * @param  @param  null  $root Always null, since this field has no parent.
     * @param  array<string, mixed>  $args The field arguments passed by the client.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Shared between all fields.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Metadata for advanced query resolution.
     * @return mixed
     */
    public function __invoke($root, array $args)
    {
        $files = $args['files'];

        return array_map([$this, 'uploadToS3'], $files);
    }

    private function uploadToS3($file)
    {
        $authenticatedUser = Auth::user();

        $src = $file->store("parts/{$authenticatedUser->id}", [
            'visibility' => 'private',
        ]);

        return [
            'src' => $src,
        ];
    }
}
