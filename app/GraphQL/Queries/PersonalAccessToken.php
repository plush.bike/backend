<?php

namespace App\GraphQL\Queries;

use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Database\Query\Builder;

class PersonalAccessToken
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke(): Builder
    {
        /** @var \App\Models\User */
        $authenticatedUser = Auth::user();

        return $authenticatedUser->tokens();
    }
}
