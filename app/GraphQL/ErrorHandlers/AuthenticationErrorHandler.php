<?php

namespace App\GraphQL\ErrorHandlers;

use Closure;
use GraphQL\Error\Error;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Execution\ErrorHandler;
use Nuwave\Lighthouse\Exceptions\AuthenticationException as LighthouseAuthenticationException;

class AuthenticationErrorHandler implements ErrorHandler
{
    public function __invoke(?Error $error, Closure $next): ?array
    {
        if (
            $error->getPrevious() instanceof LighthouseAuthenticationException
        ) {
            throw new AuthenticationException($error->getMessage());
        }

        return $next($error);
    }
}
